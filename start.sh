#!/bin/bash
set -eu

echo "=> Creating directories"
mkdir -p /app/data/ /run/calcom /run/yarn /run/cache/

APP_BASEDIR=/app/code/calcom

# https://github.com/calcom/docker/blob/main/scripts/replace-placeholder.sh
function changeURL() {
    FROM=$1
    TO=$2

    if [ "${FROM}" = "${TO}" ]; then
        echo "Nothing to replace, the value is already set to ${TO}"
        exit 0
    fi

    # Only peform action if $FROM and $TO are different.
    echo "===> Replacing all statically built instances of $FROM with $TO"

	find ${APP_BASEDIR}/apps/web/.next/ ${APP_BASEDIR}/apps/web/public -type f |
    while read file; do
        sed -i "s|$FROM|$TO|g" "$file"
    done
    echo "===> Done"
}

# https://github.com/calcom/cal.com/issues/8017
if [[ ! -f /app/data/.nextauth_secret ]]; then
    echo "==> Create NEXTAUTH secret"
    openssl rand -base64 32 > /app/data/.nextauth_secret
fi

# https://github.com/calcom/cal.com/issues/8017
if [[ ! -f /app/data/.calendso_encryption_key ]]; then
	echo "==> Create CALENDSO encryption key"
	openssl rand -base64 24 > /app/data/.calendso_encryption_key
fi

if [[ ! -f /app/data/env ]]; then
    cat > /app/data/env << EOF
# Add custom environment variables in this file

NEXT_PUBLIC_APP_NAME="${CLOUDRON_APP_DOMAIN}"
NEXT_PUBLIC_COMPANY_NAME="${CLOUDRON_APP_DOMAIN}"
EOF
fi

export DATABASE_URL="postgres://${CLOUDRON_POSTGRESQL_USERNAME}:${CLOUDRON_POSTGRESQL_PASSWORD}@${CLOUDRON_POSTGRESQL_HOST}:${CLOUDRON_POSTGRESQL_PORT}/${CLOUDRON_POSTGRESQL_DATABASE}"
export DATABASE_DIRECT_URL="${DATABASE_URL}"
export NEXTAUTH_SECRET="$(cat /app/data/.nextauth_secret)"
export CALENDSO_ENCRYPTION_KEY="$(cat /app/data/.calendso_encryption_key)"

echo "=> Merge cloudron and custom configs"
cat /app/data/env > /run/calcom/.env
cat >> /run/calcom/.env << EOF
NEXT_PUBLIC_LICENSE_CONSENT=true
NEXTAUTH_SECRET="${NEXTAUTH_SECRET}"
CALENDSO_ENCRYPTION_KEY="${CALENDSO_ENCRYPTION_KEY}"
POSTGRES_USER="${CLOUDRON_POSTGRESQL_USERNAME}"
POSTGRES_PASSWORD="${CLOUDRON_POSTGRESQL_PASSWORD}"
POSTGRES_DB="${CLOUDRON_POSTGRESQL_DATABASE}"
DATABASE_HOST="${CLOUDRON_POSTGRESQL_HOST}"
DATABASE_URL="${DATABASE_URL}"
CALCOM_TELEMETRY_DISABLED=true
EMAIL_FROM="${CLOUDRON_MAIL_FROM}"
EMAIL_FROM_NAME="${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Cal.com}"
EMAIL_SERVER_HOST="${CLOUDRON_MAIL_SMTP_SERVER}"
EMAIL_SERVER_PORT="${CLOUDRON_MAIL_SMTP_PORT}"
EMAIL_SERVER_USER="${CLOUDRON_MAIL_SMTP_USERNAME}"
EMAIL_SERVER_PASSWORD="${CLOUDRON_MAIL_SMTP_PASSWORD}"
NEXT_PUBLIC_SUPPORT_MAIL_ADDRESS="${CLOUDRON_MAIL_FROM}"
NEXT_PUBLIC_WEBAPP_URL="${CLOUDRON_APP_ORIGIN}"
ALLOWED_HOSTNAMES='"${CLOUDRON_APP_DOMAIN}"'
NEXT_PUBLIC_API_V2_URL="${CLOUDRON_APP_ORIGIN}/api/v2"
EOF

echo "==> Update site URL"
# https://github.com/calcom/docker/blob/main/scripts/start.sh
# check if calcom moved to another domain, we have to update site URLs
if [[ -f ${APP_BASEDIR}/apps/web/.next/routes-manifest.json ]]; then
    if ! grep -rl "${CLOUDRON_APP_ORIGIN}" ${APP_BASEDIR}/apps/web/.next/routes-manifest.json; then
        rm -rf ${APP_BASEDIR}/apps/web/.next/* ${APP_BASEDIR}/apps/web/public/*
    fi
fi
if [[ ! -f ${APP_BASEDIR}/apps/web/.next/routes-manifest.json ]]; then
    cp -R ${APP_BASEDIR}/apps/web/.next.orig/. ${APP_BASEDIR}/apps/web/.next
    cp -R ${APP_BASEDIR}/apps/web/public.orig/. ${APP_BASEDIR}/apps/web/public
fi
if ! grep -rl "${CLOUDRON_APP_ORIGIN}" ${APP_BASEDIR}/apps/web/.next/routes-manifest.json; then
    changeURL "http://cloudron:1337" "$CLOUDRON_APP_ORIGIN"
    changeURL "http://cloudron:5555" "$CLOUDRON_APP_ORIGIN"
fi

echo "==> Migrate DB"
cd /app/code/calcom && npx prisma migrate deploy

echo "==> Starting Cal.com"
exec yarn start --no-daemon
