FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4 AS base

WORKDIR /app/code

FROM base AS builder

ARG NEXTAUTH_SECRET=secret
ARG CALENDSO_ENCRYPTION_KEY=secret
ARG NEXT_PUBLIC_WEBAPP_URL="http://cloudron:1337"
ARG NEXT_PUBLIC_API_V2_URL="http://cloudron:5555/api/v2"

ARG CALCOM_TELEMETRY_DISABLED=1
ARG NEXT_PUBLIC_LICENSE_CONSENT=true
ARG BUILD_STANDALONE=true

# Solve JavaScript heap out of memory error - increase memory to 8GB
ARG NODE_OPTIONS="--max_old_space_size=8192"

# renovate: datasource=github-releases depName=calcom/cal.com versioning=semver extractVersion=^v(?<version>.+)$
ARG CALCOM_VERSION=5.0.19

RUN wget https://github.com/calcom/cal.com/archive/refs/tags/v${CALCOM_VERSION}.tar.gz -O - | \
    tar -xz --strip-components 1 -C /app/code

# see https://github.com/calcom/docker/blob/main/Dockerfile for reference
ENV NODE_ENV=production

RUN npx turbo prune --scope=@calcom/web --docker
RUN yarn install
RUN yarn --cwd packages/embeds/embed-core workspace @calcom/embed-core run build
RUN yarn --cwd apps/web workspace @calcom/web run build

RUN rm -rf /app/code/node_modules/.cache /app/code/.yarn/cache /app/code/apps/web/.next/cache

FROM base AS runner

ARG NEXTAUTH_SECRET=secret
ARG CALENDSO_ENCRYPTION_KEY=secret
ARG NEXT_PUBLIC_WEBAPP_URL="http://cloudron:1337"
ARG NEXT_PUBLIC_API_V2_URL="http://cloudron:5555/api/v2"

ARG CALCOM_TELEMETRY_DISABLED=1
ARG NEXT_PUBLIC_LICENSE_CONSENT=true

# see https://github.com/calcom/docker/blob/main/Dockerfile for reference
ENV NODE_ENV=production

RUN mkdir -p /app/code/calcom
WORKDIR /app/code/calcom

COPY --from=builder /app/code/packages/prisma/schema.prisma ./prisma/schema.prisma
COPY --from=builder /app/code/package.json ./
COPY --from=builder /app/code/.yarnrc.yml ./
COPY --from=builder /app/code/turbo.json ./
COPY --from=builder /app/code/.yarn ./.yarn
COPY --from=builder /app/code/yarn.lock ./yarn.lock
COPY --from=builder /app/code/node_modules ./node_modules
COPY --from=builder /app/code/packages ./packages
COPY --from=builder /app/code/apps/web ./apps/web
COPY --from=builder /app/code/i18n.json ./

RUN mv apps/web/.next apps/web/.next.orig && \
    mv apps/web/public apps/web/public.orig

RUN rm /app/code/calcom/packages/prisma/.env
RUN ln -sf /run/calcom/.env /app/code/calcom/.env

COPY start.sh /app/pkg/

CMD ["/app/pkg/start.sh"]
