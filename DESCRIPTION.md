### Overview

Cal.com is the event-juggling scheduler for everyone. Focus on meeting, not making meetings. Free for individuals.

#### Features

* Connect your calendars - Cal reads your availability from all your existing calendars ensuring you never get double booked!
* Set your availability - Set repeating schedules for the times of the day and week that you want people to be able to book you.
* Share your link - Share your short cal.com personal link and make it incredibly easy for people to book a meeting at a time that works for both of you!
* Let people book when it works for both of you
