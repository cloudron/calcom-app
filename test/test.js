#!/usr/bin/env node

'use strict';

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const ADMIN_EMAIL='admin@cloudron.local'
    const ADMIN_USERNAME='admin'
    const ADMIN_PASSWORD='ChangemeChangeme1'
    const LOCATION = process.env.LOCATION || 'test';
    const EVENT_TYPE = 'test event type cloudron 61';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function signup(email, username, password) {
        await browser.get('https://' + app.fqdn + '/auth/setup');

        await waitForElement(By.xpath('//input[@name="username"]'));
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);

        await waitForElement(By.xpath('//input[@name="username"]'));
        await browser.findElement(By.xpath('//input[@name="full_name"]')).sendKeys(username);

        await waitForElement(By.xpath('//input[@name="username"]'));
        await browser.findElement(By.xpath('//input[@name="email_address"]')).sendKeys(email);

        await waitForElement(By.xpath('//input[@name="password"]'));
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);

        await waitForElement(By.xpath('//button[contains(., "Next Step")]'));
        await browser.findElement(By.xpath('//button[contains(., "Next Step")]')).click();
        await browser.sleep(10000);

        // Choose a license
        await waitForElement(By.xpath('//button[contains(., "Next Step")]'));
        await browser.findElement(By.xpath('//button[contains(., "Next Step")]')).click();
        await browser.sleep(10000);

        // Apps
        await waitForElement(By.xpath('//button[contains(., "Finish")]'));
        await browser.findElement(By.xpath('//button[contains(., "Finish")]')).click();
        await browser.sleep(3000);

        // getting-started
        await waitForElement(By.xpath('//p[contains(., "Welcome to Cal.com")]'));
        await browser.findElement(By.xpath('//button[contains(., "Next Step")]')).click();
        await browser.sleep(3000);

        // connect Calendar
        await waitForElement(By.xpath('//button[contains(., "Next Step")]'));
        await browser.findElement(By.xpath('//button[contains(., "I\'ll connect my calendar later")]')).click();
        await browser.sleep(3000);

        // Connect your video apps
        await waitForElement(By.xpath('//button[contains(., "Set up later")]'));
        await browser.findElement(By.xpath('//button[contains(., "Set up later")]')).click();
        await browser.sleep(3000);

        // availability
        await waitForElement(By.xpath('//p[contains(., "Set your availability")]'));
        await browser.findElement(By.xpath('//button[contains(., "Next Step")]')).click();
        await browser.sleep(3000);

        // Profile
        await waitForElement(By.xpath('//button[contains(., "Finish")]'));
        await browser.findElement(By.xpath('//button[contains(., "Finish")]')).click();
        await browser.sleep(3000);

        await waitForElement(By.xpath('//h3[contains(., "Event Types")]'));
    }

    async function login(email, password) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/auth/login');
        await waitForElement(By.id('email'));
        await browser.findElement(By.id('email')).sendKeys(email);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//button[contains(., "Sign in")]')).click();
        await browser.sleep(3000);

        await dismissTimezonePopup();

        await waitForElement(By.xpath('//h3[contains(., "Event Types")]'));
    }

    async function dismissTimezonePopup() {
        if (await browser.findElements(By.xpath('//h3[contains(., "Update Timezone?")]')).then(found => !!found.length)) {
            await browser.sleep(2000);
            await browser.findElement(By.xpath('//button[contains(., "Don\'t update")]')).click()
            await browser.sleep(2000);
        }
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/auth/logout');
        await browser.sleep(3000);
        await browser.manage().deleteAllCookies();
        await waitForElement(By.xpath('//h3[contains(., "You\'ve been logged out")]'));
    }

    async function contentExists() {
        await browser.get('https://' + app.fqdn + '/event-types');
        await browser.sleep(2000);

        await dismissTimezonePopup();

        await waitForElement(By.xpath(`//a[contains(., "${EVENT_TYPE}")]`));

        await browser.findElement(By.xpath(`//a[contains(., "${EVENT_TYPE}")]`)).click();
        await browser.sleep(2000);

        await waitForElement(By.xpath(`//h3[contains(., "${EVENT_TYPE}")]`));
    }

    async function createEventType() {
        await browser.get('https://' + app.fqdn + '/event-types');
        await browser.sleep(2000);

        await dismissTimezonePopup();

        await browser.findElement(By.xpath('//button[contains(., "New")]')).click();
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//input[@name="title"]')).sendKeys(`${EVENT_TYPE}`);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[contains(., "Continue")]')).click();
        await browser.sleep(2000);

        await browser.get('https://' + app.fqdn + '/event-types');
        await waitForElement(By.xpath(`//a[@title="${EVENT_TYPE}"]`));
    }

   xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
   it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });
   it('can get app information', getAppInfo);

   it('can sign up', signup.bind(null, ADMIN_EMAIL, ADMIN_USERNAME, ADMIN_PASSWORD));
   it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
   it('can create event type', createEventType);
   it('check event type exist', contentExists);
   it('can logout', logout);

   it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

   it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));

   it('check event type exist', contentExists);
   it('can logout', logout);

   it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
   it('restore app', function () {
       const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
       execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
       execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
       getAppInfo();
       execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
   });

   it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));

   it('check event type exist', contentExists);
   it('can logout', logout);

   it('move to different location', async function () {
       await browser.get('about:blank');
       execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
   });

   it('can get app information', getAppInfo);

   it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));

   it('check event type exist', contentExists);
   it('can logout', logout);

   it('uninstall app', async function () {
       await browser.get('about:blank');
       execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
   });

    // test update
    it('install app for update', function () { execSync(`cloudron install --appstore-id com.cal.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can sign up', signup.bind(null, ADMIN_EMAIL, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('can create event type', createEventType);
    it('check event type exist', contentExists);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('check event type exist', contentExists);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
