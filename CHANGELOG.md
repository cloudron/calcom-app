[0.1.0]
* Initial version for Cal.com

[0.2.0]
* Fixup app domain usage in app

[0.3.0]
* Fix email sending
* Do not seed the database with initial data

[0.4.0]
* Update Calcom to 3.2.7.1
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.2.7.1)
* fix: Revert "feat: adds next cold start profiler (#11014)" by @hariombalhara in #11072
* fix: Fixes username invite issue by @sean-brydon in #10998
* fix: Fix avatar alligment by @sean-brydon in #11076
* fix: unauthorized error in organization by @Udit-takkar in #11039
* fix: #11077 Team event types have no padding at the bottom. by @NidhiSharma63 in #11081
* fix: Remove background colour from avatar by @sean-brydon in #11082
* chore: add Dropdown in storybook by @gitstart-calcom in #10874
* fix: Revert #10366: Undoing then redo fix differently by @emrysal in #11078

[0.5.0]
* Fixup CALENDSO_ENCRYPTION_KEY length to 24

[0.6.0]
* Update Calcom to 3.2.8
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.2.8)
* fix: Add send sms description by @AbhilashBharadwaj in #11063
* fix: support subteam in org redirection by @leog in #11080
* fix: paypal minor issues by @alannnc in #11069
* fix: Plausible App - Remove duplicate fields by @hariombalhara in #11003
* fix: Vercel preview rewrites 404s by @zomars in #11090
* chore: Add test for different layouts by @alannnc in #11091
* chore: stop using telemetry for website by @PeerRich in #11050
* fix: Booking Question Dialog: Dropdown covered by DialogFooter (CALCOM-10987) by @gitstart-calcom in #10989
* fix: dark mode icons for Riverside and Sirius Video by @CoderGhost37 in #11107
* fix: typos by @omahs in #11125
* feat: recently added apps by @Sahil25061999 in #11118
* fix: no-scrollbar for firefox by @zomars in #10964
* chore: fix typo in embed-iframe.ts by @eltociear in #10858
* style: Conformation --> Confirmation by @heathhenley in #11058
* fix: add loading to delete event dialog by @suthargk in #11102
* fix: color tokens are not used in payment form by @vineetpradhanvp in #10806
* fix: create event type by @Udit-takkar in #11046
* feat: make.com/integromat app by @aar2dee2 in #8897
* fix: teams are listed on insights but then you cant find them on settings page 11087 cal 2401 by @alannnc in #11092
* fix: installed badge by @Udit-takkar in #11131
* fix: Require an 'Email' placeholder in the 'Add guest' field. by @Madhan13K in #11123
* chore: cal.ai redirect by @PeerRich in #11130
* fix: variables for custom webhook payload by @CarinaWolli in #11135
* fix: improve UX of team invite by copy link by @Udit-takkar in #11009
* fix: missing gCal calendarId by @alannnc in #11139
* fix: FORM_SUBMITTED webhook payload change and support for Team Webhooks with it by @hariombalhara in #10986
* fix: remove double call on useSchedule() by @alannnc in #11141
* fix: #10973 back button not working as expected in embed by @vijayraghav-io in #11073
* fix: better timezones and error handling in ai tools by @DexterStorey in #11085
* fix: rescheduling in require confirmation by @Udit-takkar in #11152
* chore: removed gravatar by @PeerRich in #11153
* perf: Removed unused queries for user event types by @keithwillcode in #10568
* fix: Fix wrong footer Button in some languages by @gitstart-calcom in #10873
* fix: selected organizer when creating event by @alannnc in #11157

[0.7.0]
* Update Calcom to 3.2.9
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.2.9)
* fix: i18n issues reported by crowdin by @leog in #11160
* fix: Installing Zoho CRM by @joeauyeung in #11161
* chore: remove unused queries on user event types by @alannnc in #11159
* fix: DataTable storybook file is not rendering by @gitstart-calcom in #10984
* fix: teams page error by @Udit-takkar in #11171
* fix: event duration can be 0 by @MehulZR in #11145
* chore: add Wizard in storybook (CALCOM-10760) by @gitstart-calcom in #11166
* fix: add make invite link to setup page by @CarinaWolli in #11177
* fix: set avatar cache control by @zomars in #11163
* test: Integration tests for handleNewBooking by @hariombalhara in #11044
* fix: booking_paid webhook and added new payment metadata by @alannnc in #11093
* chore: app store improvements by @PeerRich in #11164
* fix: mobile event types and avatars by @leog in #11184
* fix: updateProfile metadata overwrite by @zomars in #11188
* chore: fixing darkmode data table by @sean-brydon in #11195
* fix: Routing Form - Show responses in the order of fields by @hariombalhara in #11143
* feat: User Avatar and Org Logo by @joeauyeung in #10700
* fix: Fixed SVG icons on light and dark mode by @pateldivyesh1323 in #11173
* feat: send new booking uid on reschedule webhook by @ologbonowiwi in #10654
* fix: bad setState call at datePicker component by @MehulZR in #11175
* chore: fixed an i18n string by @PeerRich in #11200

[0.8.0]
* Update Calcom to 3.2.10
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.2.10)
* Reduce the unhandled promise
* Connect selected calendar to credential
* Write credential to selected calendar
* Type fix
* Handle catching promises in separate threads
* Only call to list calendars if there are no credentialIds
* Changed type of IntegrationCalendar from undef to null
* Adding missing property to getting started
* Also add calendars.tsx

[0.9.0]
* Update Calcom to 3.3.0
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.3.0)
* chore: Fix overflow on invite user desc by @sean-brydon in #11320
* fix: multiple organizer address bug by @Udit-takkar in #11299
* fix: insights upgrade screen has no title and subtitle for upgrade screen by @MeenuyD in #11328
* fix: updating workflow that is active on managed event type by @CarinaWolli in #11337
* fix: org shell and org user settings avatars + org owner username by @leog in #11342
* fix: when creating new user, set email handle as username by @joeauyeung in #11238
* feat: Add more metadata to Stripe transaction by @kcrimi in #11221
* chore: Enable prettier on vitest files without causing test failures by @hariombalhara in #11190
* test: verify embed helpers by @ologbonowiwi in #11318
* fix: paypal save event type and payment by @alannnc in #11181
* fix: remove avatar form user select when loading insights by @alannnc in #11353
* feat: editmode availbility slider by @sean-brydon in #11197
* feat: getalby.com app by @PeerRich in #11347
* fix: Import prisma in handler by @sean-brydon in #11358
* chore: Outlook processBusyTimes iterate busy times once by @joeauyeung in #11350
* fix: SAML SSO - Stop relying on invitedTo column and use membership table instead by @devkiran in #11331
* fix: yarn.lock by @hariombalhara in #11364
* fix: Routing Forms - Forward Query Params and show the error message in case of validation error in headless router by @hariombalhara in #11338
* fix: Allow attendee first and last name from attendee name when name … by @ujwalkumar1995 in #11287
* fix: use selected duration when checking for conflicts for new bookings by @dihedral in #10713
* fix: Duplicate calendar events by @joeauyeung in #11327
* fix: Import prisma by @keithwillcode in #11369

[0.9.1]
* Update Calcom to 3.3.1
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.3.1)
* feat: Calendar Cache by @zomars in #11185
* chore: edit and update paypal setup instructions by @alannnc in #11325
* fix: settings/organization/members table filtering issue by @SomayChauhan in #11313
* fix: 2FA password visibility toggle show/hide state by @Chiranjeev-droid in #11361
* fix: team page data by @leog in #11382
* fix: a new email can signin with any password once it's invited to a team by @SomayChauhan in #11359
* fix: Include returns all credential fields, select excludes by @emrysal in #11383
* fix: Embed - app.cal.com instead of cal.com by @hariombalhara in #11384
* fix: Safari overflowing issues by @sean-brydon in #11385
* chore: Update GH action from crowdin to latest version by @p6l-richard in #11387
* feat(i18n): changed Japanese translations on booking page by @p6l-richard in #11388

[1.0.0]
* Update Calcom 3.3.2
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.3.2)
* fix: org username check for dynamic and signup by @leog in #11390
* fix: Email Embed slots not showing up by @hariombalhara in #11379
* fix: "scrollbar-gutter: stable" on html to prevent layout shift by @ChaudharyPradip in #11413
* fix: Messaging apps listed under conferencing tab in Installed Apps section by @hariombalhara in #11167
* feat: support firstName and lastName query params by @hariombalhara in #11420
* fix: Required "Add guests" button isn't required on attendee's end by @gitstart-calcom in #11381
* chore: app contribution guidelines by @aar2dee2 in #11416
* chore: send comment about merge conflict by @PeerRich in #11345
* test: e2e oidc by @shivamklr in #11129
* fix: re-render on insights with orgs by @alannnc in #11395
* fix: Return all responses in booking api by @kcrimi in #11284
* feat: resend invitation by @leog in #11428
* chore: slow caldav hint in description by @PeerRich in #11438
* fix: org invite by @Udit-takkar in #11356
* feat: monthly email digest by @Pradumn27 in #10621
* fix: Time Buttons Should be Scroll not the Whole Right Side by @gutogalego in #11412
* fix: unpublished org/team avatar and i18n by @leog in #11429
* feat: Sync app credentials between Cal.com & self-hosted platforms by @joeauyeung in #11059
* feat: Setups prisma accelerate by @zomars in #11324

[1.0.1]
* Update Calcom to 3.3.3
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.3.3)
* fix: subteam invite org acceptance by @leog in https://github.com/calcom/cal.com/pull/11449
* Revert "feat: Sync app credentials between Cal.com & self-hosted plat… by @zomars in https://github.com/calcom/cal.com/pull/11450
* chore: reducing team next data by @leog in https://github.com/calcom/cal.com/pull/11463
* feat: Readd app syncing by @joeauyeung in https://github.com/calcom/cal.com/pull/11453
* feat: Add 300s accelerate cache to feature flags by @emrysal in https://github.com/calcom/cal.com/pull/11464
* chore: Add login to rewrites by @emrysal in https://github.com/calcom/cal.com/pull/11466
* chore: avoid i18n strings in org profile by @leog in https://github.com/calcom/cal.com/pull/11469

[1.0.2]
* Use new base image 4.2.0

[1.0.3]
* Update Calcom to 3.3.4
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.3.4)
* fix: team members not showing up in org members list by @Udit-takkar in https://github.com/calcom/cal.com/pull/11456
* fix: remove kyc and make text messages to attendee org only by @CarinaWolli in https://github.com/calcom/cal.com/pull/11389
* fix: disable create team button for non admins by @Udit-takkar in https://github.com/calcom/cal.com/pull/11470
* chore: reduce team data footprint by @leog in https://github.com/calcom/cal.com/pull/11478
* fix: #11483 Added Loading/Progress indicator while saving the workflow by @Hitesh-sisara in https://github.com/calcom/cal.com/pull/11484
* fix: server side error on /apps by @Udit-takkar in https://github.com/calcom/cal.com/pull/11485
* chore: convert isCreateTeamButtonDisabled to bool by @Udit-takkar in https://github.com/calcom/cal.com/pull/11473
* chore: add reply_to to all booking emails by @gitstart-calcom in https://github.com/calcom/cal.com/pull/11481
* fix: auto-approve crowdin translations uploaded from GitHub by @p6l-richard in https://github.com/calcom/cal.com/pull/11490
* fix: remove stripe metadata and more logos by @leog in https://github.com/calcom/cal.com/pull/11489
* feat: Add ability to edit avatar by @samueldenzil in https://github.com/calcom/cal.com/pull/11399
* fix: #11397 request for reschedule did not cancel initial meeting by @vijayraghav-io in https://github.com/calcom/cal.com/pull/11411
* fix: The clickable area of the 'x' in add guests section is not user friendly by @gitstart-calcom in https://github.com/calcom/cal.com/pull/11434
* feat: Cal AI V1.1.0 by @DexterStorey in https://github.com/calcom/cal.com/pull/11446
* fix: Tickets are being shown in user preferred time format by @harsh11101 in https://github.com/calcom/cal.com/pull/11349
* fix: incorrect client_id parameter assignment in generating access token for webex by @Dhoni77 in https://github.com/calcom/cal.com/pull/11472
* chore: refactor app list into common component by @ty-kerr in https://github.com/calcom/cal.com/pull/9754
* feat: Zoho Calendar by @murtajaziad in https://github.com/calcom/cal.com/pull/10429
* fix: Update the oauth imports to `_utils/oauth` by @emrysal in https://github.com/calcom/cal.com/pull/11507
* chore: removed ring from org avatar by @PeerRich in https://github.com/calcom/cal.com/pull/11498
* fix: Salesforce by wrapping the refresh logic in a try/catch by @emrysal in https://github.com/calcom/cal.com/pull/11508
* fix: add a default timezone by @Udit-takkar in https://github.com/calcom/cal.com/pull/11475
* fix: icons for dark mode by @PeerRich in https://github.com/calcom/cal.com/pull/11309
* fix: username change test by @shivamklr in https://github.com/calcom/cal.com/pull/11511
* fix: verify-rerender-checkout-stripe-session by @alannnc in https://github.com/calcom/cal.com/pull/11493
* test: private team test by @Udit-takkar in https://github.com/calcom/cal.com/pull/11523
* fix: Prefill issue when name isn't present and add unit tests by @hariombalhara in https://github.com/calcom/cal.com/pull/11418
* fix: changed Japanese translations on booking page to better match the scenario. by @p6l-richard in https://github.com/calcom/cal.com/pull/11529
* chore: add dato preview secret to turbo deps by @p6l-richard in https://github.com/calcom/cal.com/pull/11535
* fix: timezone of default schedule should change if profile timezone is updated by @CarinaWolli in https://github.com/calcom/cal.com/pull/11533
* fix: Dynamic events prevent fetching all users that share a username [CAL-2409] by @joeauyeung in https://github.com/calcom/cal.com/pull/11288
* fix: form input not being saved after "back" by @G3root in https://github.com/calcom/cal.com/pull/11540
* fix: profile slug added on create your first event type dialog by @Chiranjeev-droid in https://github.com/calcom/cal.com/pull/11494
* fix: allow different casing for team by @samueldenzil in https://github.com/calcom/cal.com/pull/11546
* fix: Blank state wouldn't set 'schedule'; resulting toggle had no effect by @emrysal in https://github.com/calcom/cal.com/pull/11335
* feat: ability to reorder fields in multiple select in Routing Forms by @G3root in https://github.com/calcom/cal.com/pull/10927
* docs: Add team availability doc for API by @alishaz-polymath in https://github.com/calcom/cal.com/pull/11554
* fix: removed duplicate account delete confirmation text by @nitinpanghal in https://github.com/calcom/cal.com/pull/11520
* feat: Paid and confirmed booking should be able to be rescheduled v1 by @Ahmadkashif in https://github.com/calcom/cal.com/pull/11417
* fix: app crashing due to invalid hex by @G3root in https://github.com/calcom/cal.com/pull/11542
* fix: change localhost:3000 to Gitpod workspace URL by @annleefores in https://github.com/calcom/cal.com/pull/10470
* fix: credential sync app lookup by @keithwillcode in https://github.com/calcom/cal.com/pull/11530
* fix: #10789 Prevent Double Booking (for requests that require confirmation) by @vijayraghav-io in https://github.com/calcom/cal.com/pull/10882
* fix: update-stripe-presented-currency by @alannnc in https://github.com/calcom/cal.com/pull/11431
* fix: Booking failures when there are no destination calendars but Google Meet location by @hariombalhara in https://github.com/calcom/cal.com/pull/11574
* fix: remove cal.com before team name by @Dhoni77 in https://github.com/calcom/cal.com/pull/11544
* refactor: event type settings by @Udit-takkar in https://github.com/calcom/cal.com/pull/11539
* feat: Alby integration by @rolznz in https://github.com/calcom/cal.com/pull/11495
* fix: event-type filters by @SomayChauhan in https://github.com/calcom/cal.com/pull/11552
* chore: Improved logging booking flow by @hariombalhara in https://github.com/calcom/cal.com/pull/11543
* chore: added new cal.ai logo and screenshot by @PeerRich in https://github.com/calcom/cal.com/pull/11585
* refactor: Setting redesign by @Udit-takkar in https://github.com/calcom/cal.com/pull/11124
* fix: Embed Snippet Generator, hard crash on reload by @hariombalhara in https://github.com/calcom/cal.com/pull/11363
* chore: more cal.ai changes by @PeerRich in https://github.com/calcom/cal.com/pull/11590
* test: Tests for AppCard component by @gitstart-app in https://github.com/calcom/cal.com/pull/11500
* chore: cleanup .husky directory by @jamesgeorge007 in https://github.com/calcom/cal.com/pull/11583
* fix: install app dropdown is too large by @bhargavagonugunta in https://github.com/calcom/cal.com/pull/11562
* fix: focus on textarea by @harmanbatheja15 in https://github.com/calcom/cal.com/pull/11522
* Revert "fix: #10789 Prevent Double Booking (for requests that require confirmation)" by @zomars in https://github.com/calcom/cal.com/pull/11594

[1.0.4]
* Update Calcom to 3.3.5
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.3.5)
* feat: OAuth provider for Zapier by @CarinaWolli in https://github.com/calcom/cal.com/pull/11465
* fix: stripe selected currency by @alannnc in https://github.com/calcom/cal.com/pull/11593
* chore: cal.ai app store improvements by @PeerRich in https://github.com/calcom/cal.com/pull/11596
* fix: Add missing prisma import by @zomars in https://github.com/calcom/cal.com/pull/11598
* Fixes the dropdown issue #11597 by @bhargavagonugunta in https://github.com/calcom/cal.com/pull/11607
* docs: E2E browser installation & setting up Node by @supalarry in https://github.com/calcom/cal.com/pull/11551
* fix: Enforce CSP on /login as well by @hariombalhara in https://github.com/calcom/cal.com/pull/11610
* fix: availability for org team event by @CarinaWolli in https://github.com/calcom/cal.com/pull/11611
* fix: only set stripe app data if it requires payment by @rolznz in https://github.com/calcom/cal.com/pull/11623
* refactor: Paypal App UI & UX by @supalarry in https://github.com/calcom/cal.com/pull/11528
* fix: profile loading fix in org by @Udit-takkar in https://github.com/calcom/cal.com/pull/11618
* fix: prisma import in extract user route in Cal AI by @DexterStorey in https://github.com/calcom/cal.com/pull/11625
* refactor: to use to navigate instead of r… by @Dhoni77 in https://github.com/calcom/cal.com/pull/11602
* fix: Handle payment flow webhooks in case of event requiring confirmation by @hariombalhara in https://github.com/calcom/cal.com/pull/11458
* test: More tests in booking flow by @hariombalhara in https://github.com/calcom/cal.com/pull/11510
* fix: app that dont require setup page by @alannnc in https://github.com/calcom/cal.com/pull/11628
* fix: inverted logos and new logo for cal video by @PeerRich in https://github.com/calcom/cal.com/pull/11633
* test: Booking flow App failure and ics file tests and improved logging by @hariombalhara in https://github.com/calcom/cal.com/pull/11646
* fix: Add 240 and 480 mins. options for multiple duration events by @nicwortel in https://github.com/calcom/cal.com/pull/10663
* fix: booker skeleton layout shift by @sean-brydon in https://github.com/calcom/cal.com/pull/11653
* feat: #1490 [CAL-422] Intercom App by @vachmara in https://github.com/calcom/cal.com/pull/10714
* fix: alby logo on payment component by @rolznz in https://github.com/calcom/cal.com/pull/11678
* fix: Use only the first connected calendar as fallback to create event by @hariombalhara in https://github.com/calcom/cal.com/pull/11668
* refactor: Use template literal instead of '+' operator by @Dhoni77 in https://github.com/calcom/cal.com/pull/11444
* fix: google calendar busy times by @CarinaWolli in https://github.com/calcom/cal.com/pull/11696
* fix: rescheduled value DB update on reschedule and insights view cancelleds by @alannnc in https://github.com/calcom/cal.com/pull/11474

[1.0.5]
* Update Calcom to 3.3.6
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.3.6)
* fix: buffer dropdown text by @CarinaWolli in https://github.com/calcom/cal.com/pull/11703
* fix: typo in reporting feature sentence by @ramsaysewell in https://github.com/calcom/cal.com/pull/11685
* test: RFC: tests for EventTypeAppCardInterface (CALCOM-11005 ) by @gitstart-calcom in https://github.com/calcom/cal.com/pull/11344
* fix: fixes organization table UI flickers and adds sticky search & filter + table header by @Siddharth-2382 in https://github.com/calcom/cal.com/pull/11711
* fix: location icon doesn't change according to theme by @Ryukemeister in https://github.com/calcom/cal.com/pull/11504
* fix: remove subteam logo by @Udit-takkar in https://github.com/calcom/cal.com/pull/11715
* fix: Single Use Private link does not regenerate after use by @Dhoni77 in https://github.com/calcom/cal.com/pull/11705
* fix: add number_sms_notifications translation by @Nikeshkumar-tk in https://github.com/calcom/cal.com/pull/11724
* chore: experimental warning for recurring events by @PeerRich in https://github.com/calcom/cal.com/pull/11727
* chore: more contrast for cal video by @PeerRich in https://github.com/calcom/cal.com/pull/11729
* fix: Backwards compatibility on google reschedule/cancel by @emrysal in https://github.com/calcom/cal.com/pull/11733
* fix: check if appslug exist by @Udit-takkar in https://github.com/calcom/cal.com/pull/11735
* feat: add parentid eventtype API along with children eventTypeIds by @alishaz-polymath in https://github.com/calcom/cal.com/pull/11714

[1.0.6]
* Update Calcom to 3.3.7
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.3.7)

[1.1.0]
* Update Calcom to 3.4.1
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.4.1)

[1.1.1]
* Update Calcom to 3.4.2
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.4.2)
* fix: failing scheduleEmailReminder cron job by @CarinaWolli in #11960
* feat: (Overlay) Persist toggle option by @sean-brydon in #11961
* fix: location dropdown overflow by @PeerRich in #11967
* fix: Add /embed route for booking/[uid] by @hariombalhara in #11976

[1.1.2]
* Update Calcom to 3.4.3
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.4.3)
* fix: Handle non-org team with same slug as the organization's requestedSlug by @hariombalhara in #11996
* fix: Unable to modify the location of a booking when rescheduling by @kremedev in #11651
* fix: get correct count for team members in slider by @sean-brydon in #12017
* fix: overlay calendar modal by @sean-brydon in #12021
* fix: team availability slider overflow by @Siddharth-2382 in #12020

[1.1.3]
* Update Calcom to 3.4.4
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.4.4)
* chore: [app dir bootstrapping 4] check nullability of navigation hook return values by @grzpab in https://github.com/calcom/cal.com/pull/12005
* chore: [app dir bootstrapping 1] generate nonce with native crypto API by @grzpab in https://github.com/calcom/cal.com/pull/11969
* chore: [app dir bootstrapping 2] ensure tests do not have explicit timeouts by @grzpab in https://github.com/calcom/cal.com/pull/11970
* fix: Date add 1 day adds 24 hours, not 1 day by @emrysal in https://github.com/calcom/cal.com/pull/12019
* feat: km-localization-cambodia by @vicheanath in https://github.com/calcom/cal.com/pull/12027
* fix: allow dots in username by @Udit-takkar in https://github.com/calcom/cal.com/pull/11706
* chore: improve cal.ai not-installed message by @PeerRich in https://github.com/calcom/cal.com/pull/12022
* fix: Allow passing secret for the webhooks via API by @alishaz-polymath in https://github.com/calcom/cal.com/pull/12039
* chore: [app dir bootstrapping 3] check nullability in AppListCard by @grzpab in https://github.com/calcom/cal.com/pull/11980
* fix: Event Type header layout issues (fix-headerLayout) by @gitstart-app in https://github.com/calcom/cal.com/pull/12047
* fix: env.example requesting 24 bytes instead of 32 bytes encryption key by @mattinannt in https://github.com/calcom/cal.com/pull/12043
* fix: booking day wrong in booking list by @CarinaWolli in https://github.com/calcom/cal.com/pull/12007
* chore: removed vital.json by @PeerRich in https://github.com/calcom/cal.com/pull/12038
* test: Create E2E tests for bookings with custom/required Long Text + other questions (teste2e-longTextQuestion) by @gitstart-app in https://github.com/calcom/cal.com/pull/11559
* fix: Avatar slug and cal links for cross org users by @hariombalhara in https://github.com/calcom/cal.com/pull/12031
* feat: remove location modal in event setup by @Udit-takkar in https://github.com/calcom/cal.com/pull/11796
* fix: response size scheduleEmailReminder by @CarinaWolli in https://github.com/calcom/cal.com/pull/12057
* fix: re-render on booker by @sean-brydon in https://github.com/calcom/cal.com/pull/12058
* fix: prevents prisma idle connections by @zomars in https://github.com/calcom/cal.com/pull/12068
* chore: [app dir bootstrapping 4.1] check nullability of navigation hook return values part 2 by @grzpab in https://github.com/calcom/cal.com/pull/12065
* perf: database index on booking_status_starttime_endtime by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/12066
* fix: Teams listing and wrong avatars - Cross org boundary scenario by @hariombalhara in https://github.com/calcom/cal.com/pull/12070
* fix/profile-dont-wait-for-avatar by @sean-brydon in https://github.com/calcom/cal.com/pull/12080
* fix: Infinite loop in timezones on the negative side of UTC by @emrysal in https://github.com/calcom/cal.com/pull/12063
* feat: add oldBookingId to reschedule bookings payload by @alishaz-polymath in https://github.com/calcom/cal.com/pull/12081
* fix: typefix for webhook and rename oldBookingid to rescheduleId by @alishaz-polymath in https://github.com/calcom/cal.com/pull/12084
* fix: event type invalidation by @Dhoni77 in https://github.com/calcom/cal.com/pull/12077

[1.1.4]
* Update Calcom to 3.4.5
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.4.5)
* feat: Lock timezone on booking page by @Siddharth-2382 in https://github.com/calcom/cal.com/pull/11891
* fix: embed rewrites post dotted usernames by @zomars in https://github.com/calcom/cal.com/pull/12087
* chore: [app dir bootstrapping 5] add RootLayout by @DmytroHryshyn in https://github.com/calcom/cal.com/pull/11982
* chore: [app dir bootstrapping 6] server-side translations by @DmytroHryshyn in https://github.com/calcom/cal.com/pull/11995
* fix: Embed Plus Org - Adds missing embed route, also fixes infinite redirection for migrated user in embed by @hariombalhara in https://github.com/calcom/cal.com/pull/12071
* fix: fix the padding and position of tabs on the Team Availability page and Organisation Members page by @Siddharth-2382 in https://github.com/calcom/cal.com/pull/12078
* test: Create E2E tests for bookings with custom/required Address + other questions (teste2e-addresQuestion) by @gitstart-app in https://github.com/calcom/cal.com/pull/11563

[1.1.5]
* Update Calcom to 3.4.6
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.4.6)
* fix: pull managed event type bookings in zapier by @CarinaWolli in https://github.com/calcom/cal.com/pull/12106
* split date ranges for calling /freebusy endpoint by @CarinaWolli in https://github.com/calcom/cal.com/pull/11962
* fix: org user avatar vertical alignment by @leog in https://github.com/calcom/cal.com/pull/12110
* fix: use app.slug not hard coded zoom by @manish-singh-bisht in https://github.com/calcom/cal.com/pull/11963
* chore: new cal.ai tip by @PeerRich in https://github.com/calcom/cal.com/pull/12096
* fix: cron scheduleEmailRemider time out by @CarinaWolli in https://github.com/calcom/cal.com/pull/12108
* fix: adding managed event type to workflow by @CarinaWolli in https://github.com/calcom/cal.com/pull/12111
* test: E2E for Orgs - The beginning by @hariombalhara in https://github.com/calcom/cal.com/pull/12095
* refactor: Falling Back to FirstCalendarCredential by @joeauyeung in https://github.com/calcom/cal.com/pull/11986
* fix: padding in footer in profile by @Udit-takkar in https://github.com/calcom/cal.com/pull/12101
* fix: Skip failing tests by @hariombalhara in https://github.com/calcom/cal.com/pull/12144
* fix: Prevent possible reason behind avatar infinite redirect by @hariombalhara in https://github.com/calcom/cal.com/pull/12143
* fix: request reschedule link in email for an Org event by @hariombalhara in https://github.com/calcom/cal.com/pull/12125
* fix: Support embedding org profile page by @hariombalhara in https://github.com/calcom/cal.com/pull/12116
* fix: impersonation for orgs by @sean-brydon in https://github.com/calcom/cal.com/pull/12113
* fix: Use correct typing for totalTeamMembers by @sean-brydon in https://github.com/calcom/cal.com/pull/12152
* fix: Google Spam Polciy alert breaking embed layout in popup mode by @hariombalhara in https://github.com/calcom/cal.com/pull/12153
* fix: Check for same day, not with <= by @emrysal in https://github.com/calcom/cal.com/pull/12167
* fix(e2e): failsafe if no availabilities by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/12168
* fix: org settings for member improvements by @Udit-takkar in https://github.com/calcom/cal.com/pull/12161
* fix: cal video recording email by @Udit-takkar in https://github.com/calcom/cal.com/pull/12079
* fix: text colour for no-show acknowledgment by @Siddharth-2382 in https://github.com/calcom/cal.com/pull/12132
* fix: Auto-link credentials internally on destination calendar by @alishaz-polymath in https://github.com/calcom/cal.com/pull/12055
* chore: remove merge conflicts workflow by @PeerRich in https://github.com/calcom/cal.com/pull/12173
* test: Create E2E tests for bookings with custom/required Select + other questions (teste2e-selectQuestion) by @gitstart-app in https://github.com/calcom/cal.com/pull/11564
* chore: [CAL-2654] Broken Icons in org invitation email by @nyctonio in https://github.com/calcom/cal.com/pull/12119

[1.1.6]
* Update Calcom to 3.4.7
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.4.7)
* fix: Make sure if header is set already, it isn't set again by @hariombalhara in #12194
* fix: Event-types API endpoint Admin POST works with either userId or teamId by @alishaz-polymath in #12175
* fix: should allow org owners and admins to delete members from subteams by @SomayChauhan in #11710
* fix: Embed: data-cal-origin not used by modal(which is used by both element click popup and floating button popup) by @hariombalhara in #12075

[1.1.7]
* Update Calcom to 3.4.8
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.4.8)
* chore: delete welcome workflow by @PeerRich in #12279
* fix: payment cancel button redirects properly by @ThyMinimalDev in #12254
* fix: capitalization typo for show event type message by @Ryukemeister in #12281
* fix: tests for booking limits by @CarinaWolli in #12284
* fix: Remove count from query to update schedule by @keithwillcode in #12287
* feat: 11642 app shimmer video by @vikcodes in #12159
* fix: Recurring Booking - Check for conflicts on first 2 slots only by @hariombalhara in #11774
* fix: add missing config files to api dockerfile by @ThyMinimalDev in #12295
* fix: When Installing Calendar App, Set Primary Calendar As Selected Calendar by @joeauyeung in #12291
* feat: configure availability time picker interval by env variable by @Shpadoinkle in #12174

[1.1.8]
* Update Calcom to 3.4.9
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.4.9)
* fix: fix bookings today by @Abhinav-Developer-23 in https://github.com/calcom/cal.com/pull/12060
* fix: Fix "could not book the meeting" issue (fix-confirmBookingIssue) by @gitstart-app in https://github.com/calcom/cal.com/pull/12305
* feat: Add Sentry to edge by @keithwillcode in https://github.com/calcom/cal.com/pull/12304
* fix: add missing config files to api dockerfile by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/12318
* fix: link attendee to meeting in ZohoCRM app by @jatinsandilya in https://github.com/calcom/cal.com/pull/12200
* fix: No loader on clicking install button from apps listing page(#12253) by @Amit91848 in https://github.com/calcom/cal.com/pull/12269
* fix: Fix recurring booking failure due to rate limiting by @hariombalhara in https://github.com/calcom/cal.com/pull/12322
* fix: Broken embed in incognito of chrome by @hariombalhara in https://github.com/calcom/cal.com/pull/12311
* chore: [app dir bootstrapping 7] update middleware by @DmytroHryshyn in https://github.com/calcom/cal.com/pull/12044
* feat: smooth transitions to interactive elements by @hichemfantar in https://github.com/calcom/cal.com/pull/11983
* feat: added /enterprise upgrade tip by @PeerRich in https://github.com/calcom/cal.com/pull/12207
* fix: add teamId index on VerificationToken by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/12339
* fix: getTeamOrThrow ran on every request by @emrysal in https://github.com/calcom/cal.com/pull/12337
* fix: display organizer location after booking by @Udit-takkar in https://github.com/calcom/cal.com/pull/12088
* fix: meeting url variable in workflow notifitcations by @CarinaWolli in https://github.com/calcom/cal.com/pull/12308

[1.1.9]
* Update Calcom to 3.4.10
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.4.10)
* fix: Org: Any team URL with slug starting with d is 404 by @hariombalhara in #12302
* fix: unhandled promise rejection in scheduleWorkflowReminder by @CarinaWolli in #12301
* fix: add translations to api dockerfile by @ThyMinimalDev in #12349
* chore: variables names by @Udit-takkar in #12352
* fix: team booking page having same slug as the org by @hariombalhara in #12213
* fix: couldn't resend team member invite by @Amit91848 in #12360

[1.2.0]
* Update Calcom to 3.5.0
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.5.0)
* fix: getting shimmer app keys by @joeauyeung in #12363
* revert: "fix: create prisma client singleton with immediately invoked function" by @ThyMinimalDev in #12366
* fix: Fix 'Book a new time' link in request-reschedule for Team Event by @hariombalhara in #12261
* feat: POST/PATCH availabilities API to accept date field by @Shpadoinkle in #12238
* fix: avatar remove button by @MehulZR in #12249
* feat: Stripe paid apps flow by @exception in #12103

[1.2.1]
* Fix warnings from nextjs

[1.2.2]
* Update Calcom to 3.5.2
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.5.2)
* fix: getting-started crash and build failure by @hariombalhara in https://github.com/calcom/cal.com/pull/12506
* fix: Avatar write and unset, ensure no bad behaviour by @emrysal in https://github.com/calcom/cal.com/pull/12504

[1.2.3]
* Update Calcom to 3.5.3
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.5.3)
* chore: Remove free use of Cal.AI by @exception in https://github.com/calcom/cal.com/pull/12489
* chore: insights UI refresher by @PeerRich in https://github.com/calcom/cal.com/pull/12498
* chore: Remove cache-control and disable avatar cache by @emrysal in https://github.com/calcom/cal.com/pull/12517
* chore: avatar padding by @PeerRich in https://github.com/calcom/cal.com/pull/12515
* fix: Stripe webhook event mismatch by @exception in https://github.com/calcom/cal.com/pull/12522
* chore: sync platform branch with main by @supalarry in https://github.com/calcom/cal.com/pull/12525
* fix: platform branch sync with main by @supalarry in https://github.com/calcom/cal.com/pull/12526
* fix: platform branch sync with main by @supalarry in https://github.com/calcom/cal.com/pull/12528
* fix: OAuth URL account for dev environment by @joeauyeung in https://github.com/calcom/cal.com/pull/12530
* chore: remove dynamic loading of EventTypeDescription on event-types page by @DmytroHryshyn in https://github.com/calcom/cal.com/pull/12524
* feat: if profile only has one public event-type, redirect to it by @SomayChauhan in https://github.com/calcom/cal.com/pull/12158
* chore: fixed order of tips by @PeerRich in https://github.com/calcom/cal.com/pull/12529
* chore: Fixed a typo "tranistions" => "transitions" by @Brijendra-Singh2003 in https://github.com/calcom/cal.com/pull/12541
* revert: sync platform branch with main by @supalarry in https://github.com/calcom/cal.com/pull/12548
* refactor: invite members handler by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/12442
* fix: phone-number-input by @Amit91848 in https://github.com/calcom/cal.com/pull/12266
* fix: event-types post api endpoint not showing hosts in response data by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/12550
* fix: removing next steps by @sean-brydon in https://github.com/calcom/cal.com/pull/12549
* fix: remove duplicate key in common.json by @Udit-takkar in https://github.com/calcom/cal.com/pull/12543
* fix: small UI improvements for troubleshooter by @PeerRich in https://github.com/calcom/cal.com/pull/12535
* fix(api): Added correct include for GET /teams/{teamId}/event-types by @kaulshashank in https://github.com/calcom/cal.com/pull/12459
* fix: crash on other team members and profile page by @SomayChauhan in https://github.com/calcom/cal.com/pull/12539
* fix: Embed: Get correct snippet for Org events by @hariombalhara in https://github.com/calcom/cal.com/pull/12380
* fix: rescheduling round robin events (emails and calendar events) by @CarinaWolli in https://github.com/calcom/cal.com/pull/12469
* fix: Org Invite E2E Change Method to getOrgMembership by @joeauyeung in https://github.com/calcom/cal.com/pull/12563
* fix: fetch event type with a team permissions by @nicolls1 in https://github.com/calcom/cal.com/pull/12448
* fix: team and org invite e2e tests by @zomars in https://github.com/calcom/cal.com/pull/12566
* fix: premium username to premium username by @sean-brydon in https://github.com/calcom/cal.com/pull/12569
* feat: signup refactor by @sean-brydon in https://github.com/calcom/cal.com/pull/11421
* fix: cal video issues by @Udit-takkar in https://github.com/calcom/cal.com/pull/12546

[1.2.4]
* Update Calcom to 3.5.4
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.5.4)
* chore: refactor reschedule EventManager with changed organizer by @CarinaWolli in https://github.com/calcom/cal.com/pull/12574
* fix: Toggling really fast throws internal errors by @emrysal in https://github.com/calcom/cal.com/pull/12580
* chore: Refactor <CalendarListContainer /> by @joeauyeung in https://github.com/calcom/cal.com/pull/12388
* fix: increase Timeout invite members transaction by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/12583
* chore: add check for recording by @Udit-takkar in https://github.com/calcom/cal.com/pull/12584
* fix: selecting behaviour fixed by @Pratik-Kumar-621 in https://github.com/calcom/cal.com/pull/12578
* chore: removed last web3 references by @PeerRich in https://github.com/calcom/cal.com/pull/12540
* chore: improve logs by @Udit-takkar in https://github.com/calcom/cal.com/pull/12467
* refactor: Abstract createBooking in handleNewBooking [CAL-2619] by @joeauyeung in https://github.com/calcom/cal.com/pull/11959
* docs(.env.example): use proper openssl rand byte size by @iperzic in https://github.com/calcom/cal.com/pull/12587
* refactor: Team Creation Flow [CAL-2751] by @joeauyeung in https://github.com/calcom/cal.com/pull/12501
* fix: invite member by username by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/12591
* fix: updating workflow with new step and new active event type by @CarinaWolli in https://github.com/calcom/cal.com/pull/12592
* chore: improve invitation form validation by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/12594
* fix: improved team upgrade screen to also show unpublished teams by @PeerRich in https://github.com/calcom/cal.com/pull/12492
* Fix:use typed query initially fill default paramaters by @sean-brydon in https://github.com/calcom/cal.com/pull/12568
* fix: attempting to book meeting in the past by @CarinaWolli in https://github.com/calcom/cal.com/pull/12597
* fix: meeting ended incorrect content type by @Amit91848 in https://github.com/calcom/cal.com/pull/12484
* fix: rescheduling recurring events by @SomayChauhan in https://github.com/calcom/cal.com/pull/12567
* feat: rate limit removeMember by @sean-brydon in https://github.com/calcom/cal.com/pull/12570

[1.2.5]
* Update Calcom to 3.5.5
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.5.5)
* chore: Succession by @PeerRich in https://github.com/calcom/cal.com/pull/12605
* fix: signup nit by @sean-brydon in https://github.com/calcom/cal.com/pull/12585
* fix: Signup options are not disabled by @Pratik-Kumar-621 in https://github.com/calcom/cal.com/pull/12610
* fix: typo in @calcom/emails readme by @samyabrata-maji in https://github.com/calcom/cal.com/pull/12615
* fix: handle reschedule request for dynamic meetings by @manpoffc in https://github.com/calcom/cal.com/pull/12275
* chore: [app-router-migration-1] migrate the pages in settings/admin to the app directory by @hbjORbj in https://github.com/calcom/cal.com/pull/12561
* chore: added cursor-pointer to img upload by @PeerRich in https://github.com/calcom/cal.com/pull/12624
* feat: add clear filters option in bookings page by @varun-balani in https://github.com/calcom/cal.com/pull/12629
* feat: display long durations in hours on booking by @mikezzb in https://github.com/calcom/cal.com/pull/12631
* feat: Allow only first slot to be booked by @haranrk in https://github.com/calcom/cal.com/pull/12636
* feat: add matomo analytics app by @harshithpabbati in https://github.com/calcom/cal.com/pull/12646
* chore: Sentry Wrapper with Performance and Error Tracing by @bwoody13 in https://github.com/calcom/cal.com/pull/12642
* chore: upgrade storybook 7.6.3 + use @storybook/nextjs by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/12673
* feat: invites bypass disabled signup by @sean-brydon in https://github.com/calcom/cal.com/pull/12626
* fix: type error in settings/admin in app router by @hbjORbj in https://github.com/calcom/cal.com/pull/12638
* feat: add resend email transport by @G3root in https://github.com/calcom/cal.com/pull/12645
* fix: booking error in case of no calendar credential but stray destinationCalendar by @hariombalhara in https://github.com/calcom/cal.com/pull/12680
* test: Booking creation failure in case of stray destinationCalendar with no matching calendar credential by @hariombalhara in https://github.com/calcom/cal.com/pull/12682
* fix: fixes multiple storybook stories and updates to the new sb7 format by @stabildev in https://github.com/calcom/cal.com/pull/12683
* fix: '/slots' endpoint API fixes by @alishaz-polymath in https://github.com/calcom/cal.com/pull/12693
* fix: component styles and sign up / onboarding dark mode by @stabildev in https://github.com/calcom/cal.com/pull/12581
* fix: ignore original rescheduled booking for booking limits by @CarinaWolli in https://github.com/calcom/cal.com/pull/12625
* fix: adding team members from organization tab that alredy exist by @SomayChauhan in https://github.com/calcom/cal.com/pull/11689
* perf: Improve performance of working hours processing by @emrysal in https://github.com/calcom/cal.com/pull/12687
* chore: [app-router-migration-2] migrate trpc, ssgInit, ssrInit by @DmytroHryshyn in https://github.com/calcom/cal.com/pull/12593
* chore: auto verify OTP whenever user enters 6 digits OTP instead of waiting for them to click on verify button by @harshithpabbati in https://github.com/calcom/cal.com/pull/12326
* feat: daily webhooks by @Udit-takkar in https://github.com/calcom/cal.com/pull/12273
* fix: Edit Location modal fields truncates on mobile view by @ujjwal2505 in https://github.com/calcom/cal.com/pull/12684
* refactor: Top Banner and add google calendar credential banner by @Udit-takkar in https://github.com/calcom/cal.com/pull/12532
* fix: refactor booking details api middleware to use team member booking join by @titanventura in https://github.com/calcom/cal.com/pull/12695
* fix: rr-host booked outside of availability by @CarinaWolli in https://github.com/calcom/cal.com/pull/12704
* test: booking and duration limits e2e by @nicktrn in https://github.com/calcom/cal.com/pull/10968
* test: Make sure response is still writable before setting headers by @ouwargui in https://github.com/calcom/cal.com/pull/12478
* fix: Up libphonenumber-js to fix #12394 by @jkcs in https://github.com/calcom/cal.com/pull/12519
* docs: Remove duplicate CALENDSO key setup from README. by @bijoysijo in https://github.com/calcom/cal.com/pull/12595
* Update PULL_REQUEST_TEMPLATE.md by @hariombalhara in https://github.com/calcom/cal.com/pull/12711
* feat: add cal video logo whitelabel for organization by @Udit-takkar in https://github.com/calcom/cal.com/pull/12616
* fix: location Select auto focus by @MehulZR in https://github.com/calcom/cal.com/pull/12715
* fix: Trim spaces on datatable search key by @sean-brydon in https://github.com/calcom/cal.com/pull/12713
* fix: Hide expires on message if never expires is selected by @patrick91 in https://github.com/calcom/cal.com/pull/12723
* chore: [app-router-migration-3] apps/[slug]/index, apps/[slug]/setup by @DmytroHryshyn in https://github.com/calcom/cal.com/pull/12620
* fix: Dynamic duration was always overwritten with the default (30) by @emrysal in https://github.com/calcom/cal.com/pull/12444

[1.3.0]
* Update Calcom to 3.6.0
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.6.0)
* fix: change booking page filter ui to match Figma by @amitabh-sahu in https://github.com/calcom/cal.com/pull/12134
* fix: Disable source maps which looks to be infinitely looping by @emrysal in https://github.com/calcom/cal.com/pull/12744
* chore: [app-router-migration-4]: apps/categories page by @DmytroHryshyn in https://github.com/calcom/cal.com/pull/12619
* chore: [app-router-migration-5] apps/installed page by @DmytroHryshyn in https://github.com/calcom/cal.com/pull/12618
* chore: Wire up Sentry for API by @keithwillcode in https://github.com/calcom/cal.com/pull/12756
* fix: Allow lowercase/uppercase/mixedcase access  with org team event booking page by @hariombalhara in https://github.com/calcom/cal.com/pull/12721
* fix: checkbox color by @Udit-takkar in https://github.com/calcom/cal.com/pull/12742
* feat: mandatory email reminder for attendees with @gmail.com by @CarinaWolli in https://github.com/calcom/cal.com/pull/12747
* fix: 500 error on email conflict by @emrysal in https://github.com/calcom/cal.com/pull/12725
* chore: removed org_team_names_example_1 by @PeerRich in https://github.com/calcom/cal.com/pull/12759
* fix: datatable toolbar by @sean-brydon in https://github.com/calcom/cal.com/pull/12757
* chore: Inter 4.0 by @sean-brydon in https://github.com/calcom/cal.com/pull/12766
* fix: #11689 for other team members page by @SomayChauhan in https://github.com/calcom/cal.com/pull/12745
* fix: Disallow changing username and email in case of Organization email invite by @hariombalhara in https://github.com/calcom/cal.com/pull/12735
* feat: roam (ro.am) conferencing by @PeerRich in https://github.com/calcom/cal.com/pull/12579
* fix: Add scroll to troubleshooter sidebar by @sean-brydon in https://github.com/calcom/cal.com/pull/12785
* chore: changed checkbox color by @Manavpreeet in https://github.com/calcom/cal.com/pull/12783
* fix: padding off event user avatar by @Amit91848 in https://github.com/calcom/cal.com/pull/12733
* fix: create event type for a team that you are an owner or admin of by @nicolls1 in https://github.com/calcom/cal.com/pull/12564
* chore: hsl-update-colors by @sean-brydon in https://github.com/calcom/cal.com/pull/12199
* fix: type error by @Udit-takkar in https://github.com/calcom/cal.com/pull/12808
* fix: 404 /event-types on login by @emrysal in https://github.com/calcom/cal.com/pull/12811
* fix: email validation in user post method API by @TaduJR in https://github.com/calcom/cal.com/pull/12743
* feat: Add consistent iCalUID by @joeauyeung in https://github.com/calcom/cal.com/pull/12122
* fix: Zapier subscriber url shown in webhooks by @riddhesh-mahajan in https://github.com/calcom/cal.com/pull/12702
* fix: Enhance the Robustness of Embed Snippets for Multiple Embeds on a Single Page by @hariombalhara in https://github.com/calcom/cal.com/pull/11512

[1.3.1]
* Update Calcom to 3.6.1
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.6.1)
* chore: revert Inter 4.0 by @PeerRich in https://github.com/calcom/cal.com/pull/12839
* feat: Shows link location and respective icon in /bookings by @kart1ka in https://github.com/calcom/cal.com/pull/12760
* fix: dark mode switch color by @Udit-takkar in https://github.com/calcom/cal.com/pull/12850
* Fix: #12830 by @mrsourav1 in https://github.com/calcom/cal.com/pull/12837
* fix: A user joining from invite link of a team doesn't automatically become member of the org by @hariombalhara in https://github.com/calcom/cal.com/pull/12774
* feat: Payment success screen re-design by @sean-brydon in https://github.com/calcom/cal.com/pull/12831
* fix: only owners can award owner role in an organization. by @SomayChauhan in https://github.com/calcom/cal.com/pull/12803
* chore: Moved e2e suite to run on pushes to main by @keithwillcode in https://github.com/calcom/cal.com/pull/12819
* fix: Workflow files for E2E naming issue by @keithwillcode in https://github.com/calcom/cal.com/pull/12859
* fix: Fix the Bg color of the sign In button after clicking Fix No #12773 by @suprabhu77 in https://github.com/calcom/cal.com/pull/12833
* fix: Correctly prefill username in case of non-org email invite in an org by @hariombalhara in https://github.com/calcom/cal.com/pull/12854
* fix: join subteam as their respective organization role by @SomayChauhan in https://github.com/calcom/cal.com/pull/12216
* fix: catch error for invalid subcriber url on webhook ping test by @CarinaWolli in https://github.com/calcom/cal.com/pull/12784
* fix: [main] Signup can't be done for email matching org domain by @hariombalhara in https://github.com/calcom/cal.com/pull/12861
* fix: country code [Testing] by @Udit-takkar in https://github.com/calcom/cal.com/pull/12807
* fix: videoCallUrl missing in webhooks for event types that require confirmation by @SomayChauhan in https://github.com/calcom/cal.com/pull/12856
* fix: Across Org Scenarios - Wrong links for event and team by @hariombalhara in https://github.com/calcom/cal.com/pull/12358
* fix: Copy invite link on safari by @hariombalhara in https://github.com/calcom/cal.com/pull/12873
* feat: More admin options for organizations by @hariombalhara in https://github.com/calcom/cal.com/pull/12424
* fix: minor typo by @vnscriptkid in https://github.com/calcom/cal.com/pull/12842
* feat: meeting started webhook by @SomayChauhan in https://github.com/calcom/cal.com/pull/12764
* fix: multiple bookings created on verify-email by @SomayChauhan in https://github.com/calcom/cal.com/pull/12864
* fix: Troubleshooter - Vertical scrolling not working on left panel or calendar by @mrsourav1 in https://github.com/calcom/cal.com/pull/12792
* fix: translate boolean values in emails by @Amit91848 in https://github.com/calcom/cal.com/pull/12741
* fix: Users Cannot Be Upgraded to Owner in Organizations by @SomayChauhan in https://github.com/calcom/cal.com/pull/12737
* fix: ZodError in auth/oauth/authorize by @CarinaWolli in https://github.com/calcom/cal.com/pull/12877
* chore: User page crash on bad metadata by @emrysal in https://github.com/calcom/cal.com/pull/12858
* fix: Dynamic Group Booking link for organization by @hariombalhara in https://github.com/calcom/cal.com/pull/12825
* fix: dropdown menu awkward hover state fixed by @Manavpreeet in https://github.com/calcom/cal.com/pull/12876
* fix: button is not visible in the Getting-Started by @mrsourav1 in https://github.com/calcom/cal.com/pull/12874
* feat: Instant Meeting by @PeerRich in https://github.com/calcom/cal.com/pull/12345
* chore: [app-router-migration-6] bookings/[status] page by @DmytroHryshyn in https://github.com/calcom/cal.com/pull/12621
* chore: [app-router-migration-8] Migrate all pages in /video directory by @hbjORbj in https://github.com/calcom/cal.com/pull/12623
* chore: [app-router-migration-8.5]: add a/b test flags for apps, workflows, getting-started, settings/teams by @DmytroHryshyn in https://github.com/calcom/cal.com/pull/12797
* fix: creating workflow reminders for existing bookings when new step is added by @CarinaWolli in https://github.com/calcom/cal.com/pull/12878
* fix: only use event type's webhooks by @Udit-takkar in https://github.com/calcom/cal.com/pull/12894
* fix: managed event type string by @ritiksharmarj in https://github.com/calcom/cal.com/pull/12891
* fix: Links on avatar for org events by @hariombalhara in https://github.com/calcom/cal.com/pull/12892
* fix: Organization's Team invite emails by @SomayChauhan in https://github.com/calcom/cal.com/pull/12843
* fix: Update Event Type Pricing For Multiple Installed Payment Apps by @joeauyeung in https://github.com/calcom/cal.com/pull/12272
* chore: Reduced shard count to 5 for E2E by @keithwillcode in https://github.com/calcom/cal.com/pull/12901
* test: Seed GCal test credentials by @joeauyeung in https://github.com/calcom/cal.com/pull/12596
* fix: organization-invitation e2e tests by @CarinaWolli in https://github.com/calcom/cal.com/pull/12904
* fix: siderbar hover state by @Udit-takkar in https://github.com/calcom/cal.com/pull/12910
* fix: leading-none on labels by @sean-brydon in https://github.com/calcom/cal.com/pull/12908
* feat: Restrict organization creation to admins and then automatically verify all organizations by @SomayChauhan in https://github.com/calcom/cal.com/pull/12787
* fix: preview link for a non-org member within a team inside an org by @hariombalhara in https://github.com/calcom/cal.com/pull/12911
* test: testing workflow triggers by @CarinaWolli in https://github.com/calcom/cal.com/pull/12823
* fix: Link Provided in the Setup of PostgreSQL DB with Railway.app was wrong or not Updated by @jaskrrish in https://github.com/calcom/cal.com/pull/12915
* Fix: #12898 by @mrsourav1 in https://github.com/calcom/cal.com/pull/12919
* fix: Dont allow rescheduling for an already cancelled/rejected and thus resheduled booking by @hariombalhara in https://github.com/calcom/cal.com/pull/12926

[1.3.2]
* Update Calcom to 3.6.2
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.6.2)

[1.3.3]
* Update Calcom to 3.6.3
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.6.3)
* chore: mitigates docker rate limiting by @zomars in https://github.com/calcom/cal.com/pull/13130
* fix: Payment Page not showing by @hariombalhara in https://github.com/calcom/cal.com/pull/13146

[1.3.4]
* Update Calcom to 3.6.4
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.6.4)
* fix: Bundle analysis commenting by @keithwillcode in https://github.com/calcom/cal.com/pull/13116
* fix: org link for desktop by @PeerRich in https://github.com/calcom/cal.com/pull/13119
* fix: scheduling/canceling workflow emails in cron api call by @CarinaWolli in https://github.com/calcom/cal.com/pull/13123
* feat: OOO booking-forwarding by @alannnc in https://github.com/calcom/cal.com/pull/12653
* fix: only consider unique eventTriggers by @riddhesh-mahajan in https://github.com/calcom/cal.com/pull/13080
* chore: [app-router-migration 25] Migrate "/maintenance" page by @hbjORbj in https://github.com/calcom/cal.com/pull/13056
* chore: [app-router-migration-11] Migrate workflow page group by @DmytroHryshyn in https://github.com/calcom/cal.com/pull/12777
* chore: [app-router-migration 13] Migrate reschedule page group by @DmytroHryshyn in https://github.com/calcom/cal.com/pull/13030
* chore: [app-router-migration 19] Migrate /settings/my-account/* pages by @hbjORbj in https://github.com/calcom/cal.com/pull/13045
* chore: [app-router-migration 20] Migrate /settings/security/* pages by @hbjORbj in https://github.com/calcom/cal.com/pull/13046
* chore: [app-router-migration 22] Migrate /insights page by @hbjORbj in https://github.com/calcom/cal.com/pull/13055
* chore: Ignore admin and devops team labels by @keithwillcode in https://github.com/calcom/cal.com/pull/13162
* chore: [app-router-migration 23] Migrate the "enterprise" page by @grzpab in https://github.com/calcom/cal.com/pull/13044
* fix: Bundle analysis message by @keithwillcode in https://github.com/calcom/cal.com/pull/13161
* feat: add utcOffset in webhook payload by @jkcs in https://github.com/calcom/cal.com/pull/12709
* fix: add missing translations for workflows variables by @CarinaWolli in https://github.com/calcom/cal.com/pull/13125
* fix: fix the fluctuations in sidebar when we scroll main content by @rahilansari261 in https://github.com/calcom/cal.com/pull/13115
* fix: Use constants.ts for brand colours > db DEFAULT by @sean-brydon in https://github.com/calcom/cal.com/pull/13167
* chore: [app-router-migration 15] migrate bookings/* page group by @DmytroHryshyn in https://github.com/calcom/cal.com/pull/13038
* chore: corrected event-types icons by @PeerRich in https://github.com/calcom/cal.com/pull/13173
* fix: Oauth refreshToken endpoint for teams by @CarinaWolli in https://github.com/calcom/cal.com/pull/13159
* chore: [app-router-migration 24] migrate more page by @DmytroHryshyn in https://github.com/calcom/cal.com/pull/13054
* chore: rename 'reason' to 'cancellationReason' to match the api by @stylessh in https://github.com/calcom/cal.com/pull/13178
* fix: correctly use eventTriggers parsed and use Array.from by @sean-brydon in https://github.com/calcom/cal.com/pull/13193

[1.4.0]
* Update Calcom to 3.7.0
* * [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.7.0)
* fix: use brand accent instead of invert on current day selector by @sean-brydon in #13194
* fix: add check for already used slug by @riddhesh-mahajan in #13076
* chore: [app-router-migration 14] migrate not-found page by @DmytroHryshyn in #13032
* chore: [app-router-migration 17]: migrate payments page by @DmytroHryshyn in #13039
* fix: don't allow uid update in bookings update api by @riddhesh-mahajan in #13071
* fix: use DateRangePicker component in my-account/out-of-office by @CarinaWolli in #13207

[1.4.1]
* Update Calcom to 3.7.1
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.7.1)
* chore: add error message for no availability by @Udit-takkar in #13230
* fix: Org Migration - Team Revert should revert team members as well by @hariombalhara in #13228
* refactor: Refactor seats logic by @joeauyeung in #12905
* feat: Allow admins to filter bookings only by attendee emails by @keithwillcode in #13236
* fix: Org branding isnt centered in shell by @VishalB98 in #13237
* fix: Use Google recurring events protocol by @haranrk in #12651
* feat: impersonation improvements by @sean-brydon in #13066
* chore: Chore team metadata table + isOrganization migration from metadata by @sean-brydon in #12828
* fix: untranslated string in event by @Udit-takkar in #13247
* docs: Documentation only changes by @codesmith-emmy in #13246
* chore: Revert bundle analysis text formatting by @keithwillcode in #13256
* refactor: app-store imports webhook.ts from @calcom/web by @ThyMinimalDev in #13257

[1.4.2]
* Update Calcom to 3.7.2
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.7.2)
* chore: [app-router-migration 28] Migrate org/[orgSlug]/[user] page group by @DmytroHryshyn in https://github.com/calcom/cal.com/pull/13100
* fix: fix build error caused by 'export' keyword before getData by @DmytroHryshyn in https://github.com/calcom/cal.com/pull/13276
* feat: skip code verification for instance admin by @hariombalhara in https://github.com/calcom/cal.com/pull/13278

[1.4.3]
* Update Calcom to 3.7.6
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.7.6)

[1.4.4]
* Update Calcom to 3.7.8
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.7.8)
* fix: API v1 slot return regression fix by @alishaz-polymath in #13379
* fix: API slots endpoint return format by @zomars in #13377
* fix: UI mismatch with form state by @MehulZR in #10651
* feat: make org members list private by @Amit91848 in #13373
* feat: notion calendar by @PeerRich in #13299
* fix: sync/async calls in getSchedule by @keithwillcode in #13387

[1.4.5]
* Update Calcom to 3.7.9
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.7.9)
* fix insights page by @DmytroHryshyn in #13390
* feat: Limit the amount of days to load for a schedule by @keithwillcode in #13282

[1.4.6]
* Update Calcom to 3.7.10
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.7.10)
* fix: failing type-check in main by @CarinaWolli in https://github.com/calcom/cal.com/pull/13423
* feat: added ability to assign event to all members by @Pradumn27 in https://github.com/calcom/cal.com/pull/13219
* fix: typo in webhook delete response by @jalada in https://github.com/calcom/cal.com/pull/13409
* fix: app crash on clicking on an event by @SomayChauhan in https://github.com/calcom/cal.com/pull/13435
* fix: "Hide notes in calendar" keeps reverting to off despite saving by @SomayChauhan in https://github.com/calcom/cal.com/pull/13434
* fix: prefill avatar when signing up by @neo773 in https://github.com/calcom/cal.com/pull/13427
* fix: copy private link tooltip by @SomayChauhan in https://github.com/calcom/cal.com/pull/13437
* fix: When declining OAuth return to previous page by @joeauyeung in https://github.com/calcom/cal.com/pull/13189
* fix: destination calendar selection resetting to default on page reload by @SomayChauhan in https://github.com/calcom/cal.com/pull/13440
* fix: Deployments on main by @keithwillcode in https://github.com/calcom/cal.com/pull/13450
* fix: show leave site alert for instant meetings only by @SomayChauhan in https://github.com/calcom/cal.com/pull/13448
* fix: seated event custom questions by @MehulZR in https://github.com/calcom/cal.com/pull/12779
* fix: before/after event buffers not rendering correctly by @keithwillcode in https://github.com/calcom/cal.com/pull/13451
* fix: Adds missing rate limiting headers by @zomars in https://github.com/calcom/cal.com/pull/13430
* fix: Pay to book for an event that no longer has payments by @SomayChauhan in https://github.com/calcom/cal.com/pull/13432
* fix: make show more banner static > absolute to allow embed auto height by @sean-brydon in https://github.com/calcom/cal.com/pull/13457
* feat: salesroom app by @PeerRich in https://github.com/calcom/cal.com/pull/13277

[1.4.7]
* Update Calcom to 3.7.11
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.7.11)
* chore: fixed theme colors by @PeerRich in #13463
* fix: tailwind config for web modules by @zomars in #13466

[1.4.8]
* Update Calcom to 3.7.12
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.7.12)

[1.4.9]
* Update Calcom to 3.7.14
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.7.14)
* fix: event-type listing not showing existing event-types when an existing user is invited to an organization by @hariombalhara in https://github.com/calcom/cal.com/pull/13524
* fix: don't display setup page icon for these apps by @Udit-takkar in https://github.com/calcom/cal.com/pull/13537
* fix: Admin shouldn't be able to update the owner details by @hariombalhara in https://github.com/calcom/cal.com/pull/13534
* fix: add emailVerified in seed script by @Udit-takkar in https://github.com/calcom/cal.com/pull/13530
* fix: payload cleanup by @zomars in https://github.com/calcom/cal.com/pull/13552
* fix: Flakiness in location tests by @Udit-takkar in https://github.com/calcom/cal.com/pull/13557
* fix: 'Invalid event length' error for 1 hour dynamic events by @Nis-Han in https://github.com/calcom/cal.com/pull/13522
* fix: locale flaky tests by @Udit-takkar in https://github.com/calcom/cal.com/pull/13564
* fix: recurring round robin events by @CarinaWolli in https://github.com/calcom/cal.com/pull/13471
* fix: 2FA falky test by @Udit-takkar in https://github.com/calcom/cal.com/pull/13567
* fix: org user event types with same event slug by @CarinaWolli in https://github.com/calcom/cal.com/pull/13571

[1.4.10]
* Update Calcom to 3.7.15
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.7.15)
* perf: Load bookings used for limits in bulk by @keithwillcode in https://github.com/calcom/cal.com/pull/13474
* chore: adds failsafe auto migrations back by @zomars in https://github.com/calcom/cal.com/pull/13569
* perf: Don't process date overrides outside of requested boundaries by @keithwillcode in https://github.com/calcom/cal.com/pull/13495
* feat: integrate formbricks for team disband action by @ShubhamPalriwala in https://github.com/calcom/cal.com/pull/13362
* refactor: removed isNotAnApiCall by @zomars in https://github.com/calcom/cal.com/pull/13568
* fix: Ensure event type duplication respects private link setting by @SomayChauhan in https://github.com/calcom/cal.com/pull/13438
* fix: filtering events bug in bookings page by @SomayChauhan in https://github.com/calcom/cal.com/pull/13447
* feat: email confirmation on change by @sean-brydon in https://github.com/calcom/cal.com/pull/13443
* fix: Reduce payload in event-type/getByViewer by @hariombalhara in https://github.com/calcom/cal.com/pull/13580
* fix: Hide branding still retains the space by @laneme in https://github.com/calcom/cal.com/pull/13579
* refactor: trpc get schedule handler by @supalarry in https://github.com/calcom/cal.com/pull/13586
* fix: 2FA OTP flaky test by @Udit-takkar in https://github.com/calcom/cal.com/pull/13583
* fix: add missing title by @anikdhabal in https://github.com/calcom/cal.com/pull/13307
* fix: prevent duplicate calendar account linking by @Amit91848 in https://github.com/calcom/cal.com/pull/13310
* fix: Revert event-type profile based querying by @hariombalhara in https://github.com/calcom/cal.com/pull/13588
* test: Bookings: Add more automated tests for organization by @hariombalhara in https://github.com/calcom/cal.com/pull/13576
* fix: organization user shouldn't be asked for payment for a premium username by @hariombalhara in https://github.com/calcom/cal.com/pull/13535
* chore: refactor handle new reccuring booking by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/13597
* test: Create unit tests for the questions (teste2e-multiSelectQuestion) by @gitstart-app in https://github.com/calcom/cal.com/pull/11569
* fix: update valid_for_secs by @Udit-takkar in https://github.com/calcom/cal.com/pull/13604
* fix: Fixes teams UI load issue by @anantJjain in https://github.com/calcom/cal.com/pull/13593
* chore: upgrades boxyhq jackson by @zomars in https://github.com/calcom/cal.com/pull/13477
* fix: organizer_default_conferencing_app translatable key in app location by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/13614
* feat: round-robin priority ranking by @CarinaWolli in https://github.com/calcom/cal.com/pull/13566
* fix: Reduce payload even further. by @hariombalhara in https://github.com/calcom/cal.com/pull/13599
* chore: Remove platform from label ignore list by @keithwillcode in https://github.com/calcom/cal.com/pull/13618

[1.4.11]
* Update Calcom to 3.7.16
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.7.16)
* fix: settings ooo layout by @alannnc in https://github.com/calcom/cal.com/pull/13623
* fix: oncancel modal action week layout by @alannnc in https://github.com/calcom/cal.com/pull/13625
* fix: troubleshooter scroll by @sean-brydon in https://github.com/calcom/cal.com/pull/13631
* fix: improvements for variables in routing-from routes by @CarinaWolli in https://github.com/calcom/cal.com/pull/13624
* refactor: Bump LangChain to 0.1 & update other LangChain imports by @bracesproul in https://github.com/calcom/cal.com/pull/13622
* fix: unable to setup localy by @anikdhabal in https://github.com/calcom/cal.com/pull/13634
* fix: Errors due to namespacing of embed by @hariombalhara in https://github.com/calcom/cal.com/pull/13637
* fix: use org logo for sub teams by @sean-brydon in https://github.com/calcom/cal.com/pull/13632
* fix: Missing date ranges when specifying a specific slot time by @emrysal in https://github.com/calcom/cal.com/pull/13645

[1.4.12]
* Update Calcom to 3.7.18
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.7.18)
* refactor: migrates user password to it's own table by @zomars in https://github.com/calcom/cal.com/pull/13628
* fix: usage of CAL_URL by @Udit-takkar in https://github.com/calcom/cal.com/pull/13326
* fix: loading animation of update buttons by @moulibrota-das in https://github.com/calcom/cal.com/pull/13131
* fix: Collective Events: Duration parameter in link not working by @Nis-Han in https://github.com/calcom/cal.com/pull/13523
* fix: Add location to ics file by @joeauyeung in https://github.com/calcom/cal.com/pull/13646
* chore: Removed Skiff from the App Store by @suyash5053 in https://github.com/calcom/cal.com/pull/13650
* fix: use destination calendar email by @Udit-takkar in https://github.com/calcom/cal.com/pull/12821
* fix: AI build by @keithwillcode in https://github.com/calcom/cal.com/pull/13657
* style: Update SettingsToggle.tsx by @aaazzam in https://github.com/calcom/cal.com/pull/13660
* fix: amend user password migration by @zomars in https://github.com/calcom/cal.com/pull/13662
* fix: route always selects 'custom' after saving an event redirect by @CarinaWolli in https://github.com/calcom/cal.com/pull/13665
* fix: dont link to org url inside embeds by @PeerRich in https://github.com/calcom/cal.com/pull/13668
* revert: UserPassword by @emrysal in https://github.com/calcom/cal.com/pull/13680

[1.4.13]
* Update Calcom to 3.7.19
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.7.19)
* fix: Organizations Admin improvements by @hariombalhara in https://github.com/calcom/cal.com/pull/13656
* revert: Revert UserPassword (#13680) by @zomars in https://github.com/calcom/cal.com/pull/13685

[1.5.0]
* Update Calcom to 3.8.0
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.8.0)
* fix: Video app categories page need null safety by @A7Lavinraj in https://github.com/calcom/cal.com/pull/13674
* feat: Add Google Profile Photo when connecting Google Calendar by @zeeshanbhati in https://github.com/calcom/cal.com/pull/13627
* fix: password reset when provider is changed by @Udit-takkar in https://github.com/calcom/cal.com/pull/13706
* fix: safely delete userPassword by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/13705
* feat: store event-type filters to localStorage by @abhijeetsingh-22 in https://github.com/calcom/cal.com/pull/13407

[1.5.1]
* Update Calcom to 3.8.3
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.8.3)
* chore: Refactor feature flags by @exception in https://github.com/calcom/cal.com/pull/13734
* fix: prefillFormParams guest type by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/13798
* feat: Settings for dashboard theme that is separate from system by @sean-brydon in https://github.com/calcom/cal.com/pull/13779
* feat: signup redirect query param after onboarding by @sean-brydon in https://github.com/calcom/cal.com/pull/13761
* chore: Including original start and end dates from request in log by @keithwillcode in https://github.com/calcom/cal.com/pull/13800
* fix: No available users found near midnight by @keithwillcode in https://github.com/calcom/cal.com/pull/13801
* fix: crash on signup page while inviting a new member to an org by @SomayChauhan in https://github.com/calcom/cal.com/pull/13807
* fix: sdk action events by @Udit-takkar in https://github.com/calcom/cal.com/pull/13755
* fix: log more info on sendEmail error by @CarinaWolli in https://github.com/calcom/cal.com/pull/13813
* fix: Bookings API by @keithwillcode in https://github.com/calcom/cal.com/pull/13815
* chore: Shorten "aplicaciones instaladas" to "apps instaladas" by @baileypumfleet in https://github.com/calcom/cal.com/pull/13806
* fix: more og avatar fixes by @zomars in https://github.com/calcom/cal.com/pull/13817
* chore: removing wrong translations about {{consoleUrl}} by @PeerRich in https://github.com/calcom/cal.com/pull/13802
* feat: let org admins change usernames by @PeerRich in https://github.com/calcom/cal.com/pull/13384
* fix: Type check failure in occupiedSeats by @emrysal in https://github.com/calcom/cal.com/pull/13805
* fix: Update credentialId when null for a given selectedCalendar by @emrysal in https://github.com/calcom/cal.com/pull/13735
* chore: Added a period after undone by @Vaibhav-Kumar-K-R in https://github.com/calcom/cal.com/pull/13824

[1.5.2]
* Update Calcom to 3.8.4
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.8.4)
* fix: New Team Members Join Org as OWNER/ADMIN by @SomayChauhan in https://github.com/calcom/cal.com/pull/13822
* fix: og image by @Udit-takkar in https://github.com/calcom/cal.com/pull/13829
* test: Create E2E tests for Analytics Apps (teste2e-analyticsApps) by @gitstart-app in https://github.com/calcom/cal.com/pull/13695
* feat: demodesk app by @PeerRich in https://github.com/calcom/cal.com/pull/13733
* build: deploy script for gradual migration by @zomars in https://github.com/calcom/cal.com/pull/13834
* fix: show go back button after expiry by @Udit-takkar in https://github.com/calcom/cal.com/pull/13833
* fix: username overflow by @Shyam-Raghuwanshi in https://github.com/calcom/cal.com/pull/13832
* fix: seed script by @Udit-takkar in https://github.com/calcom/cal.com/pull/13868
* fix: video call url in event location for recurring events by @CarinaWolli in https://github.com/calcom/cal.com/pull/13842
* fix: Prevent duplicate bookings with idempotency key by @emrysal in https://github.com/calcom/cal.com/pull/13865
* fix: Limit future bookings - Default is 1 day even though on UI it is 30 days by @Pritam-sd-dev in https://github.com/calcom/cal.com/pull/13851
* fix: infinite rendering in event setup tab by @Udit-takkar in https://github.com/calcom/cal.com/pull/13877
* fix: Embed - Flickering Header by @hariombalhara in https://github.com/calcom/cal.com/pull/13880
* fix: ics-feedcalendar logo not showing by @Shyam-Raghuwanshi in https://github.com/calcom/cal.com/pull/13886
* chore: use transaction by @Udit-takkar in https://github.com/calcom/cal.com/pull/13874
* fix: missing userPrimaryEmail in workflows by @Udit-takkar in https://github.com/calcom/cal.com/pull/13875
* fix: hover state by @Udit-takkar in https://github.com/calcom/cal.com/pull/13862

[1.5.3]
* Update Calcom to 3.8.5
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.8.5)
* fix: Fix calendar event included in the workflow, doesn't show the meeting link by @asadath1395 in #13872
* fix: remove special characters from email display name by @CarinaWolli in #13876
* fix: Test flakiness due to unfilled name/email by @emrysal in #13900
* fix: text overflowing in username change popup by @niks-nikhil-anand in #13911
* fix: Currency Not Reflecting Correctly #13845 by @Pritam-sd-dev in #13847

[1.5.4]
* Update Calcom to 3.8.6
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.8.6)
* fix: workfow e2e tests by @CarinaWolli in #13938
* fix: toLowercase on emailChangeWaitingForVerification by @sean-brydon in #13940
* feat: show top banner for broken integrations [Cal 3125] by @Hitesh-Sisara in #13720
* fix: broken google account avatar fix by @therealrinku in #13892
* fix: Google Profile Picture Update Race Condition by @joeauyeung in #13711
* fix: font bold font semibold by @MysteryHawk17 in #13939
* fix: Update api image Dockerfile by @jalada in #13890

[1.5.5]
* Udpate Calcom to 3.8.7
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.8.7)
* fix: team event type error when when no one is assigned by @Udit-takkar in https://github.com/calcom/cal.com/pull/13888
* fix: add the X-Cal-Signature-256 header to MEETING_ENDED webhook requests by @swain in https://github.com/calcom/cal.com/pull/13986
* fix: Email Embed for team event types by @Udit-takkar in https://github.com/calcom/cal.com/pull/13990
* chore: added new .env for script tags by @PeerRich in https://github.com/calcom/cal.com/pull/13765
* chore: Reduce insights range to 7 days by @keithwillcode in https://github.com/calcom/cal.com/pull/13991
* fix: unable to remove single recurring instance from gCal by @Amit91848 in https://github.com/calcom/cal.com/pull/13960
* feat: added banner for orgs by @PeerRich in https://github.com/calcom/cal.com/pull/13644
* feat: SCIM 2.0 Automatic User provisioning by @devkiran in https://github.com/calcom/cal.com/pull/11169
* fix: missing videoCallUrl in metadata for static link based video apps by @CarinaWolli in https://github.com/calcom/cal.com/pull/13995
* fix: slugify from creating double+ dashes by @sean-brydon in https://github.com/calcom/cal.com/pull/14002
* fix: Banner text not showing correctly in light mode by @kedar1514 in https://github.com/calcom/cal.com/pull/13962
* build: Adds noImplicitAny to tsconfig by @zomars in https://github.com/calcom/cal.com/pull/13997
* fix: add future team members toggle by @SomayChauhan in https://github.com/calcom/cal.com/pull/14008
* fix: duplicate event types by @Udit-takkar in https://github.com/calcom/cal.com/pull/14007
* build: adds action to manually remove faulty cache entries by @zomars in https://github.com/calcom/cal.com/pull/14017
* fix: Implement new avatars in insights by @emrysal in https://github.com/calcom/cal.com/pull/13996
* fix: avatar upload flakyness by @zomars in https://github.com/calcom/cal.com/pull/14021
* feat: SCIM Group Provisioning To Teams Within An Org by @joeauyeung in https://github.com/calcom/cal.com/pull/13850

[1.5.6]
* Update Calcom to 3.8.9
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.8.9)
* fix: event length in webhook payload by @CarinaWolli in https://github.com/calcom/cal.com/pull/14039
* fix: metadata for seats in webhooks by @CarinaWolli in https://github.com/calcom/cal.com/pull/14025
* chore: add pt-BR translations by @candouss in https://github.com/calcom/cal.com/pull/14022
* fix: Create team events using admin token by @asadath1395 in https://github.com/calcom/cal.com/pull/14044
* revert: "fix: view team bookings (#13591)" by @keithwillcode in https://github.com/calcom/cal.com/pull/14066

[1.6.0]
* Update Calcom to 3.9.1
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.9.1)
* fix: hide hidden fields on booking page by @Udit-takkar in #13843
* chore: update to see webhook creation error handling to see error in prod by @sean-brydon in #14071
* chore: run migrations conditionally by @zomars in #13925
* fix: Remove the right date override on removal by @emrysal in #13988
* fix: Do not refund payments when cancelling bookings by @joeauyeung in #13924
* refactor: removes deprecated customPrisma for API by @zomars in #14069
* fix: Get dsyncData based on database by @joeauyeung in #14067
* feat: lock event types for org users by @sean-brydon in #14000
* fix: A bug fix by @Gabbar-v7 in #14081
* fix: Exclude current rescheduled booking from limit by @Amit91848 in #13870
* fix: don't show 'send event details to' for team events by @CarinaWolli in #14093
* fix: Avoid CSP related warnings in console on booking pages. by @hariombalhara in #14072
* refactor: Moving credential syncing to API by @joeauyeung in #13963
* fix: deprecated code in new credential sync endpoints by @zomars in #14116
* fix: email embed displaying wrong date by @Udit-takkar in #14128
* fix: failing webhookTriggers cron job by @CarinaWolli in #14104
* chore: update github actions for platform by @ThyMinimalDev in #14130
* feat: support for platform wide webhooks by @SomayChauhan in #14092
* feat: add 2fa removal for admins by @sean-brydon in #14126
* fix: date picker arrow positioning by @Udit-takkar in #14129
* fix: dynamic event length change on reschedule by @Shobana7 in #14110
* fix: booking limits for team event types by @CarinaWolli in #14135
* feat: golden verified badge for org members by @PeerRich in #14131
* fix: fix indicator size by @sean-brydon in #14125
* fix: pass org slug to useSchedule by @sean-brydon in #14138
* feat: display org banner on personal event types by @Udit-takkar in #14101
* style: Provide Visual Feedback to Users Upon Button Click to Indicate Processing Time #13553 by @murtaza030 in #14137
* chore: improve apple calendar message by @Udit-takkar in #14139
* fix: Crash of event types page if the upId is incorrect by @emrysal in #14136
* fix: Minor UI fix to team profile / fallback to parent org logo by @emrysal in #14143

[1.6.1]
* Update Calcom to 3.9.2
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.9.2)
* fix: Some tests weren't running by @emrysal in #14145
* fix: timezone display on booking page to reflect event availability timezone by @Shaik-Sirajuddin in #14127
* fix: Adding 'hosts' instead of 'users' by @emrysal in #14105
* fix: email displayed as organizer eventtype settings by @Amit91848 in #14102
* feat: platform atoms + api v2 by @ThyMinimalDev in #14106
* chore: hide org banner i hideEventTypeDetails is true by @PeerRich in #14119
* chore: Platform SDK by @exception in #14154
* chore: adds Intercom admin link by @zomars in #14146
* fix: migrated users admin update by @sean-brydon in #14152
* fix: improve defined slot start times by @CarinaWolli in #14107
* fix: Broken reschedule link in email created by workflow for team event by @Pritam-sd-dev in #13893
* fix: url query params dynamic url server side app router by @alannnc in #14076
* fix: Allow team admins to create managed events by @alishaz-polymath in #14165
* fix: rescheduling in round robin by @Udit-takkar in #14109

[1.6.2]
* Update Calcom to 3.9.3
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.9.3)
* chore: bump GitHub postgres v2 by @emrysal in https://github.com/calcom/cal.com/pull/14220
* feat: Migrate all old avatars to 'avatars' table by @emrysal in https://github.com/calcom/cal.com/pull/14170
* fix: add missing multiple_duration_timeUnit translation in zh-CN by @ZacharyJia in https://github.com/calcom/cal.com/pull/14212
* feat: platform endpoints to fetch/cancel/reschedule bookings and hooks by @Ryukemeister in https://github.com/calcom/cal.com/pull/14164
* chore(ci): Add nextjs framework env vors to @calcom/website in turbo.json by @p6l-richard in https://github.com/calcom/cal.com/pull/14231
* chore: added vercel env to turbo repo by @PeerRich in https://github.com/calcom/cal.com/pull/14238
* fix: embed env var EnvrionmentPlugin config by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14225
* chore: rename platform endpoints provider/gcal by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14248
* chore: rename platform /me endpoints by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14249
* feat: implement serverside search ƒor org user table by @sean-brydon in https://github.com/calcom/cal.com/pull/14234
* feat: cal.ai enterprise phone calls by @PeerRich in https://github.com/calcom/cal.com/pull/14100
* refactor: platform timezone endpoint by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14250
* chore: add styling to examples app by @Ryukemeister in https://github.com/calcom/cal.com/pull/14252
* chore: Select organization fields to prevent logo import by @emrysal in https://github.com/calcom/cal.com/pull/14255
* fix: Organization check by @keithwillcode in https://github.com/calcom/cal.com/pull/14251
* fix(translations/de): typo in limit_booking_only_first_slot by @philippdormann in https://github.com/calcom/cal.com/pull/14244
* chore: adds useful SQL scripts for devops by @zomars in https://github.com/calcom/cal.com/pull/14068
* fix: No handlers found for ics-feed by @Amit91848 in https://github.com/calcom/cal.com/pull/14123
* refactor: prisma platform tables use pascal case by @supalarry in https://github.com/calcom/cal.com/pull/14263
* refactor: OAuth creation form hide apps permissions by @supalarry in https://github.com/calcom/cal.com/pull/14264
* fix: Bookings Teams filter shows double label by @Shyam-Raghuwanshi in https://github.com/calcom/cal.com/pull/14239
* chore: Skip broken out-of-office tests by @emrysal in https://github.com/calcom/cal.com/pull/14286
* chore: rewrite api.cal.com/v2 to api v2 by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14285

[1.6.3]
* Update Calcom to 3.9.4
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.9.4)
* fix: cancellation seems to be using slug instead of title by @Amit91848 in https://github.com/calcom/cal.com/pull/14195
* chore: add more duration options for multiple duration events by @Udit-takkar in https://github.com/calcom/cal.com/pull/14162
* fix: Show proper actionable error in case the team can't be published by @hariombalhara in https://github.com/calcom/cal.com/pull/14283
* feat: v2 event-types POST, PATCH, DELETE endpoints by @supalarry in https://github.com/calcom/cal.com/pull/14237
* chore: remove cal-ai from nav by @PeerRich in https://github.com/calcom/cal.com/pull/14293
* fix: Sentry DNS -> Sentry DSN by @exception in https://github.com/calcom/cal.com/pull/14295
* fix: Booker atom modal not rendering for mobile devices by @Ryukemeister in https://github.com/calcom/cal.com/pull/14278
* fix: Platform Hotfixes by @exception in https://github.com/calcom/cal.com/pull/14297
* fix: Typo in OAuth clients by @keithwillcode in https://github.com/calcom/cal.com/pull/14292
* chore: SDK Fixes and add swagger docs by @exception in https://github.com/calcom/cal.com/pull/14218

[1.6.4]
* Update Calcom to 3.9.5
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.9.5)
* chore: Move avatars using cursor by @emrysal in https://github.com/calcom/cal.com/pull/14257
* chore: Platform tidy up unused variables, etc by @exception in https://github.com/calcom/cal.com/pull/14289
* fix: org guard did not have user org profile + missing db read in selectedCalendarRepo by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14303
* feat: new org onboarding wizard by @PeerRich in https://github.com/calcom/cal.com/pull/13139
* chore: publish atoms on npm and bump version by @supalarry in https://github.com/calcom/cal.com/pull/14309
* fix: API builds after rename by @zomars in https://github.com/calcom/cal.com/pull/14319
* chore: icon cleanup by @zomars in https://github.com/calcom/cal.com/pull/13770
* fix: Add adminReview option to allow impersonating org members by @hariombalhara in https://github.com/calcom/cal.com/pull/14275
* feat: list managed users of an oauth client by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14330
* fix: pass begin_message for updating LLM by @Udit-takkar in https://github.com/calcom/cal.com/pull/14327
* chore: downgrade react query atoms by @supalarry in https://github.com/calcom/cal.com/pull/14328
* revert: chore downgrade react query atoms by @supalarry in https://github.com/calcom/cal.com/pull/14332
* docs(contrib): update docs for yarn.lock management by @p6l-richard in https://github.com/calcom/cal.com/pull/14230
* chore: Remove payment keys from appKeysSchema by @joeauyeung in https://github.com/calcom/cal.com/pull/14298
* fix: Show proper error when API_V2_URL variable is missing by @hariombalhara in https://github.com/calcom/cal.com/pull/14269
* chore: added email and company form to cal.ai form by @PeerRich in https://github.com/calcom/cal.com/pull/14306
* fix: refresh access token expiry 60m by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14331
* fix: correct limit amount in api middleware by @sean-brydon in https://github.com/calcom/cal.com/pull/14333
* feat: implement turnstile on signup by @sean-brydon in https://github.com/calcom/cal.com/pull/13913
* fix: urls, gcal check, oauth client controller by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14335
* revert: OAuth creation form hide apps permissions by @supalarry in https://github.com/calcom/cal.com/pull/14336
* docs: v2 event types swagger by @supalarry in https://github.com/calcom/cal.com/pull/14296
* feat: added Rating Template to Workflows by @PeerRich in https://github.com/calcom/cal.com/pull/14215
* chore: teams filter improvements by @emrysal in https://github.com/calcom/cal.com/pull/14315
* fix: improve api/cron/webhookTriggers endpoint by @CarinaWolli in https://github.com/calcom/cal.com/pull/14188

[1.6.5]
* Update Calcom to 3.9.6
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.9.6)
* chore: bump atoms package to 1.0.11 by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14344
* fix: remove port in prod url apiv2 by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14343
* fix: scope oauth managed users to oauth client id to avoid username / emails conflicts by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14347
* fix: create platform handler isPlatform flag by @supalarry in https://github.com/calcom/cal.com/pull/14288
* fix: "Checkbox" custom field throws error by @Amit91848 in https://github.com/calcom/cal.com/pull/14339
* fix: change identity provider on password reset by @Udit-takkar in https://github.com/calcom/cal.com/pull/14326
* chore: bump atoms package version 1.0.12 by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14428
* fix: filter and listMember handler by @sean-brydon in https://github.com/calcom/cal.com/pull/14429
* feat: retell AI webhook url by @Udit-takkar in https://github.com/calcom/cal.com/pull/14430
* ci: adds api v1 builds to required checks by @zomars in https://github.com/calcom/cal.com/pull/14345
* fix: v1 tsconfig decorators by @supalarry in https://github.com/calcom/cal.com/pull/14454
* chore: change timezone format and update webhook by @Udit-takkar in https://github.com/calcom/cal.com/pull/14433
* fix: Team Invite Email Text formatting by @hariombalhara in https://github.com/calcom/cal.com/pull/14434
* fix: Empty "Add guests" email input is blocking form submission by @Amit91848 in https://github.com/calcom/cal.com/pull/14442
* fix: Add missing import for Icon component by @kart1ka in https://github.com/calcom/cal.com/pull/14440

[1.6.6]
* Update Calcom to 3.9.7
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.9.7)
* fix: Return team event type by @keithwillcode in https://github.com/calcom/cal.com/pull/14464
* fix: Allow unpublished org to serve redirected links plus other org onboarding fixes by @hariombalhara in https://github.com/calcom/cal.com/pull/14337

[1.6.7]
* Update Calcom to 3.9.8
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.9.8)
* fix: Ensure the session is invalidated during booking-seats test by @emrysal in https://github.com/calcom/cal.com/pull/14478
* chore: add instruction in recording email by @Udit-takkar in https://github.com/calcom/cal.com/pull/14424
* refactor: Add crm type to app credentials by @joeauyeung in https://github.com/calcom/cal.com/pull/14147
* fix: Remove exports in barrel file which are causing prisma import on client side by @hariombalhara in https://github.com/calcom/cal.com/pull/14475
* fix: isTeamEvent would eval to null when undefined by @emrysal in https://github.com/calcom/cal.com/pull/14480
* fix: Remove logo from getPublicEvent response by @emrysal in https://github.com/calcom/cal.com/pull/14488
* feat: ooo-v2 by @alannnc in https://github.com/calcom/cal.com/pull/13621
* refactor: v2 docs by @supalarry in https://github.com/calcom/cal.com/pull/14446
* fix: unnecessary access token guard on public endpoint for slots by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14492
* fix: [CAL-422] Intercom App add link by @vachmara in https://github.com/calcom/cal.com/pull/14496
* fix: view team bookings by @SomayChauhan in https://github.com/calcom/cal.com/pull/14079
* chore: AtomsWrapper to scope css by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14490
* feat: new App install flow by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/11975
* fix: org-redirect-url-admin-update by @sean-brydon in https://github.com/calcom/cal.com/pull/14493
* fix: private link for a team event gives 404 by @Amit91848 in https://github.com/calcom/cal.com/pull/14473
* feat: SDK managed users + PLA fixes by @exception in https://github.com/calcom/cal.com/pull/14495
* fix: Fix broken locale in Availability Settings title by @emeeran in https://github.com/calcom/cal.com/pull/14501
* chore: bump atoms to 1.0.13 by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14506
* fix: OOO for team events by @CarinaWolli in https://github.com/calcom/cal.com/pull/14503
* fix(platform): missing string literal opening by @johnforte in https://github.com/calcom/cal.com/pull/14463
* feat: Implement the ability to login via multiple means when signed up via google/non-cal providers by @asadath1395 in https://github.com/calcom/cal.com/pull/14082
* fix: correct language is used when booking with a modal + location icon visible in booker when using light mode by @Jojo14234 in https://github.com/calcom/cal.com/pull/14280
* feat: In Embed, Fire 'routed' event when Routing Form routes by @hariombalhara in https://github.com/calcom/cal.com/pull/14470
* fix: platform booking endpoints should be public by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14509
* fix: platform gcal connect reconnect credentials by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14513
* fix: Broken SSR for event booking pages by @hariombalhara in https://github.com/calcom/cal.com/pull/14497
* fix: test by @hariombalhara in https://github.com/calcom/cal.com/pull/14535
* feat: add redirect uris for booking success, cancel and reschedule for a platform user by @Ryukemeister in https://github.com/calcom/cal.com/pull/14518
* fix: Redirect only those apps that extend 'EventType' to the new apps install flow by @SomayChauhan in https://github.com/calcom/cal.com/pull/14527
* feat: always show intercom on desktop by default (hidden on mobile) by @SomayChauhan in https://github.com/calcom/cal.com/pull/14423
* revert: "fix: Redirect only those apps that extend 'EventType' to the new apps install flow" by @exception in https://github.com/calcom/cal.com/pull/14539
* fix: Invalid 403 forbidden for event-types GET by @keithwillcode in https://github.com/calcom/cal.com/pull/14528
* revert: "feat: new App install flow" by @exception in https://github.com/calcom/cal.com/pull/14540

[1.6.8]
* Update Calcom to 3.9.9
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v3.9.9)
* fix: State Inconsistency issue on webhook edit modal by @Amit91848 in #14544
* chore: Publish new versions of embeds by @hariombalhara in #14543
* fix: atoms package types were not exported by @ThyMinimalDev in #14546
* fix: Remove hardcoded stripe.. by @emrysal in #14549
* chore: Bump SDK version 1.0.1 by @exception in #14524
* fix: [CAL-3374] Says Number is not Verified, but it is Verified by @Chiranjeev-droid in #14531
* fix: v2 create schedule with availabilities by @supalarry in #14551
* feat: Delete/Create WebhookScheduledTriggers for existing bookings by @Amit91848 in #14121
* feat: travel schedules to schedule timezone changes by @CarinaWolli in #14512
* chore: enable platform emails by @ThyMinimalDev in #14471
* fix: Cal Video Links Expire After 14 Days by @joeauyeung in #14557
* fix: update playwright config to allow permissions by @sean-brydon in #14526
* feat: add classNames prop to Booker atom for passing external styles by @Ryukemeister in #14270
* fix: remove prefix /api from apiv2 by @ThyMinimalDev in #14559
* chore: adds maintenance mode for API v1 by @zomars in #14555
* revert: "fix: view team bookings (#14079)" by @keithwillcode in #14561
* chore: fix class type and bump atoms version by @ThyMinimalDev in #14562
* refactor: v2 event types endpoint paths by @supalarry in #14545
* feat: return isPlatform boolean to detect if user is a platform user by @Ryukemeister in #14570
* fix: Integrate maintenance mode with current middleware by @keithwillcode in #14571

[1.7.0]
* Update Calcom to 4.0.0
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.0.0)
* chore: remove /ee from apiv2 urls by @ThyMinimalDev in #14568
* fix: only call useMe if access token is set by @ThyMinimalDev in #14576
* fix: Production deploys by @keithwillcode in #14578
* fix: Remove PageWrapper by @emrysal in #14579

[1.7.1]
* Update Calcom to 4.0.1
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.0.1)
* fix: removing stripe results in the event-types page loading indefintely by @SomayChauhan in #14583
* fix: conditionally remove fixed prop if we're in embed by @sean-brydon in #14586
* fix: acme.cal.local crashes due to missing requestedSlug in seed by @emrysal in #14592
* fix: email not sending with : in from name by @CarinaWolli in #14594
* fix: intercom visible on mobile by @SomayChauhan in #14563
* fix: Re-instate hasEmailBeenChanged before sending email by @emrysal in #14596
* feat: use experimental optimizePackageImports for @calcom/ui by @sean-brydon in #14587
* chore: org signup for admins min 1 org user, for self-serve 30 by @PeerRich in #14567
* feat: update platform oauth client form by @ThyMinimalDev in #14542

[1.7.2]
* Update Calcom to 4.0.2
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.0.2)
* fix: API v2 recurring spelling by @keithwillcode in #14620
* fix: Hide org tab when you are team admin by @sean-brydon in #14627
* fix: add new styling for emoji links by @CarinaWolli in #14481
* chore: settings design match by @sean-brydon in #14630
* fix: use 'success' value for better accuracy by @chronark in #14624
* feat: AI Transcribe by @exception in #14140
* fix: 500 on forbidden showing up in logs by @emrysal in #14636
* fix: v2 app.rewrites.middleware.ts by @supalarry in #14626
* fix: add linting exception for website by @zomars in #14645
* chore: atoms 1.0.29 + fix typing issues by @ThyMinimalDev in #14633
* feat: booker atom custom location by @supalarry in #14635
* fix: Namespaced react embeds not working with Floating and Element Click Popups by @hariombalhara in #13386
* chore: atoms 1.0.30 by @ThyMinimalDev in #14650
* fix: v2 e2e tests by @supalarry in #14655
* fix: api v2 - platform users can disable emails and display their links by @ThyMinimalDev in #14604
* fix: managed users check if exist by @ThyMinimalDev in #14649
* feat: Tasker by @zomars in #14180
* chore: Remove all avatar/logo references by @emrysal in #14532
* fix: only boot intercom if .env variable is set by @SomayChauhan in #14656
* feat: include org role in org session data by @sean-brydon in #14652
* chore: Add self serve variable in tests by @hariombalhara in #14665
* chore: better string for "Create an Out of Office" by @PeerRich in #14647
* fix: remove away view by @CarinaWolli in #14672
* fix: get llm response schema by @Udit-takkar in #14670
* chore: Removing away (DB related, not slot related) from code by @emrysal in #14673
* fix: credit card icon by @PeerRich in #14678
* fix: managed type url in orgs by @Udit-takkar in #14698
* fix: show toast in center of screen by @guvvanch in #14690
* fix: tooltip text on Create event type dialog by @Tasztalos69 in #14681
* fix: Private settings of the event disabled after making changes to your event by @SomayChauhan in #14628
* fix: Date Override Modal Trims Date Picker by @smk1993 in #14680
* fix: Update visibility of team members based on user role by @sean-brydon in #14629
* feat: updated the useage of listCurrent in the app and use org role from session by @sean-brydon in #14653
* fix: send completed_onboarding to intercom by @SomayChauhan in #14703
* fix: Managed event isn’t showing in the upcoming bookings when filtered by @Amit91848 in #14552
* fix: invalid Cancelled / Rescheduled downstream calendar block by @emrysal in #14700
* fix: Avatar API isn't ran at signup handler by @emrysal in #14675

[1.7.3]
* Update Calcom to 4.0.3
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.0.3)
* fix: browser back button not working by @abhijeetsingh-22 in https://github.com/calcom/cal.com/pull/13345
* fix: Add navigatedToBooker event and fix infinite loop of events by @hariombalhara in https://github.com/calcom/cal.com/pull/14694
* feat: improve error handling for signup events by @sean-brydon in https://github.com/calcom/cal.com/pull/14668
* chore: "toggle transcription" to "Cal.ai" by @PeerRich in https://github.com/calcom/cal.com/pull/14710
* chore: EventManager add cancelEvent method by @joeauyeung in https://github.com/calcom/cal.com/pull/14261
* feat: SMS/Whatsapp to attendee for teams by @CarinaWolli in https://github.com/calcom/cal.com/pull/14648
* chore: v4.0.2 by @zomars in https://github.com/calcom/cal.com/pull/14724
* fix: Improve French translations by @keithwillcode in https://github.com/calcom/cal.com/pull/14718
* feat: Add app store entry for "Autocheckin" (Challenge/Bounty) by @pgvr in https://github.com/calcom/cal.com/pull/14683
* fix: settings layout for org admin by @Udit-takkar in https://github.com/calcom/cal.com/pull/14723

[1.7.4]
* Update Calcom to 4.0.4
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.0.4)
* fix: booker atom not resetting month query param by @supalarry in https://github.com/calcom/cal.com/pull/14715
* fix: booker atom reschedule data by @supalarry in https://github.com/calcom/cal.com/pull/14714
* fix: booker atom month reset by @supalarry in https://github.com/calcom/cal.com/pull/14732
* fix: Check permissions for team OOO entries by @zomars in https://github.com/calcom/cal.com/pull/14736
* chore: bump atoms 1.0.35 and wrap context by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14744
* fix: oAuthClient Creation Form by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14750
* fix: Credential Syncing Improvements by @hariombalhara in https://github.com/calcom/cal.com/pull/14588
* feat: SDK README docs. by @exception in https://github.com/calcom/cal.com/pull/14761
* chore: SDK cleanup by @exception in https://github.com/calcom/cal.com/pull/14763

[1.7.5]
* Update Calcom to 4.0.6
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.0.6)
* feat: send organization_id to intercom by @SomayChauhan in https://github.com/calcom/cal.com/pull/14811
* chore: Bring back next-18next.config.js by @exception in https://github.com/calcom/cal.com/pull/14841
* fix: availability.e2e.ts - Date Overrides by @CarinaWolli in https://github.com/calcom/cal.com/pull/14845
* chore: Add default to match migration for Platform Billing by @exception in https://github.com/calcom/cal.com/pull/14847
* chore: small UI fix for insights by @PeerRich in https://github.com/calcom/cal.com/pull/14839
* test: add unit tests for sms sending by @CarinaWolli in https://github.com/calcom/cal.com/pull/14737
* chore: Allow disabling attendee emails if SMS_ATTENDEE is a part of the workflow by @joeauyeung in https://github.com/calcom/cal.com/pull/14806
* chore: fixed enterprise warning and description by @PeerRich in https://github.com/calcom/cal.com/pull/14791
* chore: Attach unique request ids by @exception in https://github.com/calcom/cal.com/pull/14857
* chore: don't try to increase usage for legacy plans by @exception in https://github.com/calcom/cal.com/pull/14856
* fix: Snippet path in preview by @hariombalhara in https://github.com/calcom/cal.com/pull/14830

[1.7.6]
* Update Calcom to 4.0.7
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.0.7)
* fix: Speed up booking time by @hariombalhara in https://github.com/calcom/cal.com/pull/14809
* feat: orgs trigger alert when loading a calendar and no availability is found by @sean-brydon in https://github.com/calcom/cal.com/pull/14796
* fix: Sender ID info not shown on hover by @Amit91848 in https://github.com/calcom/cal.com/pull/14846
* feat: Add reschedule information with booking Read by @Amit91848 in https://github.com/calcom/cal.com/pull/14853
* fix: flaky webhook e2e test by @CarinaWolli in https://github.com/calcom/cal.com/pull/14850
* fix: UI breaking in AddActionDialog fixed by @raza001 in https://github.com/calcom/cal.com/pull/14883
* test: Google CalendarService test by @hariombalhara in https://github.com/calcom/cal.com/pull/14793
* fix: Support adding a non-existent email as the owner of the organization by @hariombalhara in https://github.com/calcom/cal.com/pull/14569
* fix: Booking Cancelled Webhook - Organiser webhook payload is missing id. #14697 by @vikaspatil0021 in https://github.com/calcom/cal.com/pull/14745
* fix: api v2 docs in prod by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14898
* chore: Use better buildjet hardware for e2e by @keithwillcode in https://github.com/calcom/cal.com/pull/14835
* chore: Use 4vCPU BuildJet hardware for type checks by @keithwillcode in https://github.com/calcom/cal.com/pull/14900
* fix: UI breaking in AddActionDialog on reducing browser window height by @utkershrajvenshi in https://github.com/calcom/cal.com/pull/14892
* chore: centered cal video logo by @PeerRich in https://github.com/calcom/cal.com/pull/14896
* chore: Cache cityTimezones for Platform by @exception in https://github.com/calcom/cal.com/pull/14823
* fix: Sender ID info not being shown on 'info' hover in 'add action' flow by @Amit91848 in https://github.com/calcom/cal.com/pull/14879
* fix: zod error message and missing translation by @anikdhabal in https://github.com/calcom/cal.com/pull/14787
* chore: atoms v1.0.40 by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14901
* fix: windows app styling by @PeerRich in https://github.com/calcom/cal.com/pull/14805
* fix: date override modal by @sean-brydon in https://github.com/calcom/cal.com/pull/14854
* feat: posthog app added by @vikaspatil0021 in https://github.com/calcom/cal.com/pull/14801
* feat: "Add an Override" calendar starts with the day specified in general settings by @shaik-zeeshan in https://github.com/calcom/cal.com/pull/14663
* test: Test the scenario where resourceOwner.id is null by @hariombalhara in https://github.com/calcom/cal.com/pull/14816
* fix: remove global twilio variable by @CarinaWolli in https://github.com/calcom/cal.com/pull/14907
* chore: Add build of API v2 to PR checks by @keithwillcode in https://github.com/calcom/cal.com/pull/14902
* fix: reschedule with google meet by @Udit-takkar in https://github.com/calcom/cal.com/pull/14730
* fix: slot check by @sean-brydon in https://github.com/calcom/cal.com/pull/14917
* feat: atoms team booking by @supalarry in https://github.com/calcom/cal.com/pull/14525
* fix: add prisma client generated types to atoms package by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14916
* feat: cal-3452 parameter for impersonation by @sean-brydon in https://github.com/calcom/cal.com/pull/14605
* chore: Async scalars & less queries due to grouping by @emrysal in https://github.com/calcom/cal.com/pull/14904
* feat: redirect after booking: option to forward parameters by @Amit91848 in https://github.com/calcom/cal.com/pull/14235
* chore: Removed duplicate production build by @keithwillcode in https://github.com/calcom/cal.com/pull/14836
* fix: Caching key for CI database by @keithwillcode in https://github.com/calcom/cal.com/pull/14932

[1.7.7]
* Update Calcom to 4.0.8
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.0.8)
* fix: pass query params to dynamic meetings by @sean-brydon in https://github.com/calcom/cal.com/pull/14920
* feat: Added admin-api page by @PeerRich in https://github.com/calcom/cal.com/pull/14890
* feat: handleCreateBooking for platform booker by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14939
* fix: redis cache key overlap by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14938
* feat: Redirect when event type does not match booking type by @emrysal in https://github.com/calcom/cal.com/pull/14460
* chore: Update PR template to include more mandatory tasks by @keithwillcode in https://github.com/calcom/cal.com/pull/14941
* fix: sync book event form values with bookingData by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14945
* fix: attempt to fix flaky e2e tests by @CarinaWolli in https://github.com/calcom/cal.com/pull/14949
* fix: Only able to create one webhook per event type by @Amit91848 in https://github.com/calcom/cal.com/pull/14922
* fix: managed user timeZone, timeFormat and weekStart during creation by @supalarry in https://github.com/calcom/cal.com/pull/14955
* fix: buffer times in handleNewBooking by @CarinaWolli in https://github.com/calcom/cal.com/pull/14871
* fix: gcal save incomplete app credentials by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/14962
* feat: platform onboarding flow and dashboard by @Ryukemeister in https://github.com/calcom/cal.com/pull/14721
* fix: save workflow button not visible on dev env by @mhetreayush in https://github.com/calcom/cal.com/pull/14910
* chore: v4.0.8 by @zomars in https://github.com/calcom/cal.com/pull/14965

[1.7.8]
* Update Calcom to 4.0.9
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.0.9)
* fix: api v2 hotfix google calendar, exception details, billing check guard by @ThyMinimalDev in #14967
* fix: Show error log in case of /event-types redirect by @hariombalhara in #14960
* chore: fixed giphy size by @PeerRich in #14973
* chore: dashboard fixes by @Ryukemeister in #14968
* fix: Release new versions for embed by @hariombalhara in #14958
* feat: orgs create dynamic events from org member list by @sean-brydon in #14912
* fix: Fixed email embed for team when specific times are chosen embeds the wrong URL by @vikrant845 in #14975
* fix: SAML fixes for uppercase email & GOOGLE → SAML idp switch by @alishaz-polymath in #14971
* fix: meeting ended and started webhooks trigger by @CarinaWolli in #14864
* fix: Allow less than 30 seats for orgs by @hariombalhara in #14995
* fix: add retryCount to workflowReminder by @CarinaWolli in #14943

[1.8.0]
* Update Calcom to 4.1.0
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.1.0)
* chore: more fields event-types api v2 by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/15031
* fix: disable useGetOAuthClientManagedUsers if clientId not defined by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/15040
* fix: invalid type of enable_transcription_storage by @Udit-takkar in https://github.com/calcom/cal.com/pull/15049
* chore: Allow disabling email sending without errors by @emrysal in https://github.com/calcom/cal.com/pull/15051
* chore: prop disableEditableHeading in availability settings by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/15041
* fix: Dynamic (Group) meetings showing one person avatar on the double by @Amit91848 in https://github.com/calcom/cal.com/pull/15055
* fix: hosts not set when duplicating team event-type by @anikdhabal in https://github.com/calcom/cal.com/pull/15012

[1.8.1]
* Update Calcom to 4.1.1
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.1.1)
* fix: platform onboarding improvements by @Ryukemeister in https://github.com/calcom/cal.com/pull/15053
* feat: added BAA to App Store by @PeerRich in https://github.com/calcom/cal.com/pull/14894
* refactor: api v2 schedules by @supalarry in https://github.com/calcom/cal.com/pull/14870
* fix: terms and conditions text by @Udit-takkar in https://github.com/calcom/cal.com/pull/15027
* fix: Add the 'cal-video' location only if it doesn't already exist by @SomayChauhan in https://github.com/calcom/cal.com/pull/15038
* fix: disable emails for platform by @Ryukemeister in https://github.com/calcom/cal.com/pull/15089
* fix: Invalid phone error on prefill with plus(+) sign by @hariombalhara in https://github.com/calcom/cal.com/pull/15046
* fix: Collective event types with seats - hosts count towards the seat count by @Amit91848 in https://github.com/calcom/cal.com/pull/14930
* chore: Add test for booking cancelled webhook by @Amit91848 in https://github.com/calcom/cal.com/pull/14940
* fix: Prevent 500 error when fetching more than 3000 bookings from an user by @zomars in https://github.com/calcom/cal.com/pull/15099
* chore: api-v2 slotInterval, afterEventBuffer, beforeEventBuffer min 0 by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/15039
* fix: upgrade deprecated github actions by @anikdhabal in https://github.com/calcom/cal.com/pull/14998
* chore: API Build cache in CI pipeline by @utkershrajvenshi in https://github.com/calcom/cal.com/pull/14929
* fix: rate limiting improvements by @CarinaWolli in https://github.com/calcom/cal.com/pull/15095
* chore: added google reviews by @PeerRich in https://github.com/calcom/cal.com/pull/15113
* chore: fixed a few cal.ai ui issues and cal.video logo by @PeerRich in https://github.com/calcom/cal.com/pull/15044
* feat: add autoscroll in mobile by @Udit-takkar in https://github.com/calcom/cal.com/pull/15124
* fix: daily buffer in getServerSideProps by @Udit-takkar in https://github.com/calcom/cal.com/pull/15093
* fix: resolve labeler workflow failure by @anikdhabal in https://github.com/calcom/cal.com/pull/15110
* feat: Future Limit - Toggle to exclude unavailable days by @hariombalhara in https://github.com/calcom/cal.com/pull/14915
* chore: Updated labeler to v5 by @emrysal in https://github.com/calcom/cal.com/pull/15127
* chore: hide consumer navigation in platform admin dropdown by @PeerRich in https://github.com/calcom/cal.com/pull/15117
* fix: onboarding flow styling by @Ryukemeister in https://github.com/calcom/cal.com/pull/15111
* revert: refactor: api v2 schedules by @supalarry in https://github.com/calcom/cal.com/pull/15130
* feat: Workflow — Send Email to Specific Email by @Amit91848 in https://github.com/calcom/cal.com/pull/14815
* fix: Adds missing dependecy by @zomars in https://github.com/calcom/cal.com/pull/15139
* fix: return 200 status code by @Udit-takkar in https://github.com/calcom/cal.com/pull/15134
* feat(i18n): finish pt-br translation by @amirelemam in https://github.com/calcom/cal.com/pull/15141
* refactor: v2 origin check error message by @supalarry in https://github.com/calcom/cal.com/pull/15143
* chore: Downgrade 8 vCPU actions to 4 by @keithwillcode in https://github.com/calcom/cal.com/pull/15147
* fix: Add organization logo to org dynamic events by @emrysal in https://github.com/calcom/cal.com/pull/15082
* chore: Add integration tests runner by @keithwillcode in https://github.com/calcom/cal.com/pull/15148
* fix: app crash on accessing certains pages of organization/teams when user not part of any organization or team by @Shaik-Sirajuddin in https://github.com/calcom/cal.com/pull/15106
* chore: skips flaky future limits test by @zomars in https://github.com/calcom/cal.com/pull/15149
* feat: Add Organization Admin scope to API V1 by @alishaz-polymath in https://github.com/calcom/cal.com/pull/14860

[1.8.2]
* Update Calcom to 4.1.3
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.1.3)
* fix: Make pagination optional in Bookings API by @alishaz-polymath in #15163
* chore: Moved legacy rewrites from afterFiles to beforeFiles by @zomars in #15164
* fix: if we go to event-types/[id]?tabName=apps after installing an app to an organisation it shows 2 apps by @Amit91848 in #15166
* fix: Use origin as provided in HeadSeo. origin would be different for Domain cases by @hariombalhara in #14935
* feat: v2 API enable updating event-type bookingFields by @supalarry in #15172

[1.8.3]
* Update Calcom to 4.1.4
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.1.4)
* chore: Bring back merge queues by @keithwillcode in https://github.com/calcom/cal.com/pull/15177
* fix: Required check for CI by @keithwillcode in https://github.com/calcom/cal.com/pull/15181
* chore: Skip flaky tests that have been failing often by @keithwillcode in https://github.com/calcom/cal.com/pull/15161
* fix: Org Seeder - to better represent production scenario and fix admin org delete cleanup by @hariombalhara in https://github.com/calcom/cal.com/pull/15157
* fix: signing_up_terms i18n by @Udit-takkar in https://github.com/calcom/cal.com/pull/15156
* feat: events apps new install flow by @SomayChauhan in https://github.com/calcom/cal.com/pull/14616

[1.8.4]
* Update Calcom to 4.1.5
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.1.5)
* chore: return empty array if no transcription found by @Udit-takkar in #15185
* fix: styles for the schedule component, make it mobile-friendly by @helgastogova in #15178
* fix(atoms): fix max-height bug for the Booker component by @helgastogova in #15195
* fix: attendeeSeatId is not being returned on webhook response for events with seats by @Amit91848 in #15184
* fix: Profile page crash due to invalid topLevelElement by @emrysal in #15198
* chore: added event-types link to settings by @PeerRich in #14274

[1.8.5]
* Update Calcom to 4.1.8
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.1.8)
* fix: add redirectUrl to /eventTypes page as well by @SomayChauhan in #15274
* feat: Add framework to send all events fired for embed to Analytics Apps by @hariombalhara in #15173
* fix: Typo in deleteCredential handler by @joeauyeung in #15281
* fix: 404 collisions by @keithwillcode in #15249

[1.8.6]
* Update Calcom to 4.1.9
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.1.9)
* feat: ability to assign people to round robin from org members by @Amit91848 in #14199
* fix: Kbar component styles and improve transitions by @hichemfantar in #15283
* feat: outlook 365 calendar endpoints by @Ryukemeister in #15077
* chore: added admin API docs by @PeerRich in #15297
* fix: Double encoding in addAppMutation by @joeauyeung in #15303
* fix: no rating submitted text fixed by @smitgol in #15007

[1.8.7]
* Update Calcom to 4.1.10
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.1.10)
* feat: org-wide webhooks by @kart1ka in #15144
* chore: Remove DeploySentinel by @emrysal in #15328
* fix: install flow: wrong org team avatars by @Amit91848 in #15334
* fix: Wrong error message when inviting by @hariombalhara in #15332

[1.8.8]
* Update Calcom to 4.1.11
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.1.11)

[1.8.9]
* Update Calcom to 4.1.12
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.1.12)

[1.9.0]
* Update Calcom to 4.2.0
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.2.0)

[1.9.1]
* Update Calcom to 4.2.1
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.2.1)
* feat: add date overrides to availability settings atom by @Ryukemeister in #15459
* fix: event type uses calvideo as location instead of user default by @Shaik-Sirajuddin in #15442
* fix: Remove comment about private api for now. by @sean-brydon in #15493
* fix: correctly assign status code on rate limit error by @sean-brydon in #15435
* chore: auth on api-v2 with api-key by @ThyMinimalDev in #15455

[1.9.2]
* Update Calcom to 4.2.2
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.2.2)

[1.9.3]
* Update Calcom to 4.2.3
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.2.3)

[1.9.4]
* Update Calcom to 4.2.4
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.2.4)

[1.9.5]
* Update Calcom to 4.2.5
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.2.5)

[1.10.0]
* Update Calcom to 4.3.0
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.3.0)

[1.10.1]
* Update Calcom to 4.3.1
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.3.1)

[1.10.2]
* Possible fix for embedding

[1.10.3]
* Update Calcom to 4.3.2
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.3.2)

[1.10.4]
* Update Calcom to 4.3.3
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.3.3)

[1.10.5]
* Update Calcom to 4.3.4
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.3.4)

[1.10.6]
* Update Calcom to 4.3.5
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.3.5)

[1.10.7]
* Update Calcom to 4.3.6
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.3.6)
* fix: Remove missing users from assignment by @alishaz-polymath in https://github.com/calcom/cal.com/pull/16115
* fix: use organization default fixedPrice if self-serve/default price is passed in by @sean-brydon in https://github.com/calcom/cal.com/pull/15966
* fix: display skeleton loader by @Udit-takkar in https://github.com/calcom/cal.com/pull/16114
* feat: create new markNoShow endpoint by @Udit-takkar in https://github.com/calcom/cal.com/pull/15738
* chore: Remove cal.ai email assistant by @PeerRich in https://github.com/calcom/cal.com/pull/16003
* fix: uncaught errors in PaymentService by @zomars in https://github.com/calcom/cal.com/pull/16104
* feat: Add upcoming bookings feature to GET /api/bookings endpoint by @ImBIOS in https://github.com/calcom/cal.com/pull/16060
* fix: Hide personal booking theme when in an organization by @sean-brydon in https://github.com/calcom/cal.com/pull/16117
* fix: SCIM improvements by @joeauyeung in https://github.com/calcom/cal.com/pull/16103
* fix: Unable to submit booking form if Attendee Phone is used and then booking Fields are saved by @vijayraghav-io in https://github.com/calcom/cal.com/pull/15742
* fix: Better UI for the setup page, When trying to reach an app that hasn't been already installed under an account by @Mihir867 in https://github.com/calcom/cal.com/pull/16121
* feat: Allow cancellation & reschedule on past bookings by @alishaz-polymath in https://github.com/calcom/cal.com/pull/16118
* chore: oAuth Client Guard apv2 by @ThyMinimalDev in https://github.com/calcom/cal.com/pull/16130
* fix: use singleton in license key service by @sean-brydon in https://github.com/calcom/cal.com/pull/16131

[1.11.0]
* Update Calcom to 4.4.2
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.4.2)

[1.11.1]
* Update Calcom to 4.4.4
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.4.4)
* fix: delete attribute member relation if no input is provided by @sean-brydon in #16298
* fix: Migrates E2E reports to external repo by @zomars in #16326
* revert: "fix: uncaught types errors in invite members team handler (#1… by @emrysal in #16322
* chore: rename calendar settings atom by @Ryukemeister in #16329
* fix: visiting organization without access shows empty page by @Shaik-Sirajuddin in #15103
* fix: fixing i18n key `signing_up_terms` is lost by @nusr in #16338
* fix: `multiple_duration_timeUnit_short` Chinese translation by @nusr in #16339

[1.11.2]
* Update Calcom to 4.4.5
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.4.5)

[1.11.3]
* Update Calcom to 4.4.6
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.4.6)

[1.11.4]
* Update Calcom to 4.4.7
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.4.7)

[1.11.5]
* Update Calcom to 4.4.8
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.4.8)
* fix: to show updated values in eventTypes list by @vijayraghav-io in #16569
* fix: prevent creating multiple platform orgs by @ThyMinimalDev in #16564
* fix: to allow paypal install on team when user already installed by @vijayraghav-io in #16553
* perf: Insights - put teamId filter first by @keithwillcode in #16581
* fix: booking response field's label is not translated by @anikdhabal in #16582
* revert: booking rejection reason from booking success page by @anikdhabal in #16584
* chore: App router migration (org/[orgSlug]/instant-meeting), add missing page by @hbjORbj in #16448
* perf: Server-Side Data Fetching in App Router: /availability by @hbjORbj in #16536

[1.11.6]
* Update Calcom to 4.4.9
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.4.9)
* fix: API V1 slots add explicit orgSlug null fallback by @alishaz-polymath in #16593
* fix: availability/[schedule] by @hbjORbj in #16598
* feat: booking with phone number by @Udit-takkar in #14461
* chore: Remove passing optional prisma select prop to Repository classes by @hbjORbj in #16600
* feat: update v2 calendar services logic to handle multiple calendars by @Ryukemeister in #16478
* fix(ally): changed unsemantic button by @mfranzke in #16588
* fix: (managed-event-booking): slug conflict handling by @abhijeetsingh-22 in #16540
* feat: update translations by @calcom-bot in #16603

[1.12.0]
* Update Calcom to 4.5.0
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.5.0)

[1.12.1]
* Update Calcom to 4.5.1
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.5.1)

[1.12.2]
* Update Calcom to 4.5.2
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.5.2)

[1.12.3]
* Update Calcom to 4.5.6
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.5.6)

[1.12.4]
* Update Calcom to 4.5.9
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.5.9)

[1.12.5]
* Update Calcom to 4.5.11
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.5.11)

[1.13.0]
* Update Calcom to 4.6.1
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.1)

[1.13.1]
* Update Calcom to 4.6.2
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.2)

[1.13.2]
* Update Calcom to 4.6.3
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.3)

[1.13.3]
* Update Calcom to 4.6.5
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.5)

[1.13.4]
* Update Calcom to 4.6.13
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.13)

[1.13.5]
* Update Calcom to 4.6.14
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.14)

[1.13.6]
* Update Calcom to 4.6.15
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)


[1.13.7]
* Update Cal.com to 4.6.16
* revert: "perf: Cache app store imports ([#&#8203;17425](https://github.com/calcom/cal.com/issues/17425))" by [@&#8203;keithwillcode](https://github.com/keithwillcode) in https://github.com/calcom/cal.com/pull/17428
* fix: no availability email frequency fix by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/17430
* fix: update profile photo logic in google cal integration by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17438
* perf: google cal profile photo by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17441
* fix: create a wrapper for google calendar fetching by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17440
* perf: Add more specific spans to getBusyTimes by [@&#8203;keithwillcode](https://github.com/keithwillcode) in https://github.com/calcom/cal.com/pull/17437
* fix: docs mdx syntax errors by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/17450
* fix: payments tab typo for platform by [@&#8203;Ryukemeister](https://github.com/Ryukemeister) in https://github.com/calcom/cal.com/pull/17451
* fix: filter by team url by [@&#8203;Udit-takkar](https://github.com/Udit-takkar) in https://github.com/calcom/cal.com/pull/17456
* feat: allow users to generate csv tables for org members by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17458
* chore: remove no option text by [@&#8203;Udit-takkar](https://github.com/Udit-takkar) in https://github.com/calcom/cal.com/pull/17457
* fix: ensure fetching all members before exporting csv by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17461
* perf: Remove extra spans by [@&#8203;keithwillcode](https://github.com/keithwillcode) in https://github.com/calcom/cal.com/pull/17464
* chore: v4.6.16 by [@&#8203;keithwillcode](https://github.com/keithwillcode) in https://github.com/calcom/cal.com/pull/17465
[1.13.8]
* Update cal.com to 4.6.23
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* fix: [#&#8203;17602](https://github.com/calcom/cal.com/issues/17602) by [@&#8203;AnkityadavIITR](https://github.com/AnkityadavIITR) in https://github.com/calcom/cal.com/pull/17604
* feat: v2 multiple length event types and their bookings by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/17598
* feat: v2 confirm / decline booking endpoints by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/17578
* chore: update Instant Meeting Error Message by [@&#8203;PeerRich](https://github.com/PeerRich) in https://github.com/calcom/cal.com/pull/17617
* fix: mark guest are no show by [@&#8203;Udit-takkar](https://github.com/Udit-takkar) in https://github.com/calcom/cal.com/pull/17588
* feat: update translations via [@&#8203;replexica](https://github.com/replexica) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/17619
* fix: Refactor publicPageUrl to use getBookerBaseUrlSync like elsewhere by [@&#8203;emrysal](https://github.com/emrysal) in https://github.com/calcom/cal.com/pull/17607

[1.13.9]
* Update cal.com to 4.6.24
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* chore: Cleanup Close.com by [@&#8203;zomars](https://github.com/zomars) in https://github.com/calcom/cal.com/pull/16929
* feat: Add API v2 logs to axiom by [@&#8203;keithwillcode](https://github.com/keithwillcode) in https://github.com/calcom/cal.com/pull/17595
* chore: upgrade `@vercel/og` lib by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17503

[1.14.0]
* Update cal.com to 4.7.0
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* feat(CAL-4730): add `sk-SK` language by [@&#8203;maxprilutskiy](https://github.com/maxprilutskiy) in https://github.com/calcom/cal.com/pull/17652
* fix: z index issue on dropdown menu by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/17656
* feat: exclude emails and domains by [@&#8203;VK-RED](https://github.com/VK-RED) in https://github.com/calcom/cal.com/pull/17591
* fix: Meeting not created in HubSpot for contacts by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/17661
* fix: filter cal video logo by [@&#8203;Udit-takkar](https://github.com/Udit-takkar) in https://github.com/calcom/cal.com/pull/17663
* feat: Implements maxLeadThreshold by [@&#8203;emrysal](https://github.com/emrysal) in https://github.com/calcom/cal.com/pull/17643

[1.14.1]
* Update cal.com to 4.7.1
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* chore: Remove playwright install API v2 E2E by [@&#8203;keithwillcode](https://github.com/keithwillcode) in https://github.com/calcom/cal.com/pull/17677
* perf: optimize team admin check queries by [@&#8203;zomars](https://github.com/zomars) in https://github.com/calcom/cal.com/pull/17681
* fix: Hotfix/ Fix contact owner routing by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/17684
* test: findTeamMemberEmailFromCrm by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/17685
* chore: Release v4.7.1 by [@&#8203;emrysal](https://github.com/emrysal) in https://github.com/calcom/cal.com/pull/17686

[1.14.2]
* Update cal.com to 4.7.2
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* fix: large org routing insights UI issues by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/17687
* fix: routing column filters by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/17688
* perf: optimize organization admin check by [@&#8203;zomars](https://github.com/zomars) in https://github.com/calcom/cal.com/pull/17683
* chore: Found a critical index missing on profileId by [@&#8203;emrysal](https://github.com/emrysal) in https://github.com/calcom/cal.com/pull/17690

[1.14.3]
* Update cal.com to 4.7.3
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* feat: Add link column to CSV file for org members by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17695
* feat: AI description - DB model + frontend + backend (fetch only) by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17651
* chore: v4.7.3 by [@&#8203;keithwillcode](https://github.com/keithwillcode) in https://github.com/calcom/cal.com/pull/17701

[1.14.4]
* Update cal.com to 4.7.4
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* feat: Added more supported languages by [@&#8203;keithwillcode](https://github.com/keithwillcode) in https://github.com/calcom/cal.com/pull/17702
* feat: update translations via [@&#8203;replexica](https://github.com/replexica) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/17704

[1.14.5]
* Update cal.com to 4.7.5
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* fix: cron api key checks by [@&#8203;zomars](https://github.com/zomars) in https://github.com/calcom/cal.com/pull/17730
* chore: booking status reporting by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/17733
* fix: platform libraries build vite.config by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/17743

[1.14.6]
* Update cal.com to 4.7.6
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* fix: PostHog event tracking by [@&#8203;nizzyabi](https://github.com/nizzyabi) in https://github.com/calcom/cal.com/pull/17653
* fix: fix wrong dates on OOO edit dialog by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/17736
* fix: Better error handling for user session by [@&#8203;zomars](https://github.com/zomars) in https://github.com/calcom/cal.com/pull/17753
* chore: routing table nits by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/17738
* fix: upgrade prismock to 1.33.4 by [@&#8203;vijayraghav-io](https://github.com/vijayraghav-io) in https://github.com/calcom/cal.com/pull/17735
* feat: Attribute filter/Segment for Round Robin hosts by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/17361
* chore: Add support for new docker compose command to run V2 API by [@&#8203;asadath1395](https://github.com/asadath1395) in https://github.com/calcom/cal.com/pull/17744
* feat: add text filter on org member list by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/17632
* feat: v2 expose booking metadata by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/17708
* feat: update translations via [@&#8203;replexica](https://github.com/replexica) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/17757
* fix: Log error correctly in login flow by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/17755
* fix: common schedule toggle for team event in never uncheckable by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/17759
* feat: Don't remove hosts when disabling 'Add all team members' by [@&#8203;Amit91848](https://github.com/Amit91848) in https://github.com/calcom/cal.com/pull/17747
* chore: v4.7.6 by [@&#8203;zomars](https://github.com/zomars) in https://github.com/calcom/cal.com/pull/17766

[1.14.7]
* Update cal.com to 4.7.7
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* feat: install gcal + gvideo by default for google signups by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17773
* fix: no availability on reschedule with load balancing by [@&#8203;CarinaWolli](https://github.com/CarinaWolli) in https://github.com/calcom/cal.com/pull/17770
* fix: temporarily revert stricter schema so we can handle invalid DB e by [@&#8203;emrysal](https://github.com/emrysal) in https://github.com/calcom/cal.com/pull/17774
* feat: Record assignment reasons for routing form and reassignment by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/17767
* feat: Record Salesforce ownership assignment by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/17775
* fix: extract clientId header manually apiv2 by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/17783
* fix: short text booking questions does not save when we click add by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/17778

[1.14.8]
* Update cal.com to 4.7.10
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* fix: improvements for canceling workflow reminders by [@&#8203;CarinaWolli](https://github.com/CarinaWolli) in https://github.com/calcom/cal.com/pull/17807
* fix: Conditionally get the first assignment reason by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/17808
* fix: v2 booking confirmation emails not sent by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/17764
* feat: Render assignment reason on routing form report by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/17791
* feat: added customClassnames for event-recurring tab by [@&#8203;SomayChauhan](https://github.com/SomayChauhan) in https://github.com/calcom/cal.com/pull/17551
* feat: Expose booking created at in v2 API response by [@&#8203;asadath1395](https://github.com/asadath1395) in https://github.com/calcom/cal.com/pull/17452
* feat: added customClassnames to event-availability tab by [@&#8203;SomayChauhan](https://github.com/SomayChauhan) in https://github.com/calcom/cal.com/pull/17268
* revert: "feat: install gcal + gvideo by default for google signups ([#&#8203;1](https://github.com/calcom/cal.com/issues/1) by [@&#8203;emrysal](https://github.com/emrysal) in https://github.com/calcom/cal.com/pull/17803

[1.14.9]
* Update cal.com to 4.7.11
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* feat: past and overlapping ooo by [@&#8203;Amit91848](https://github.com/Amit91848) in https://github.com/calcom/cal.com/pull/17373
* feat: added custom classnames to event-assignment tab atom by [@&#8203;SomayChauhan](https://github.com/SomayChauhan) in https://github.com/calcom/cal.com/pull/17527
* feat: added custom classnames for event-advanced tab by [@&#8203;SomayChauhan](https://github.com/SomayChauhan) in https://github.com/calcom/cal.com/pull/17479
* feat: added custom classnames for event-limits tab by [@&#8203;SomayChauhan](https://github.com/SomayChauhan) in https://github.com/calcom/cal.com/pull/17478

[1.14.10]
* Update cal.com to 4.7.12
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* feat: AI description - Backend for adding translation objects to EventType model by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17657
* fix: outdated/broken og preview image for org events by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17812
* fix: UI issue in Phone Input dropdown by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17837
* fix: Salesforce owner check, handle no contact email by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/17834
* feat: update translations via [@&#8203;replexica](https://github.com/replexica) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/17842
* fix: oauth client users controller permissions by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/17822
* fix: send attendee email with event updates of reassignment by [@&#8203;CarinaWolli](https://github.com/CarinaWolli) in https://github.com/calcom/cal.com/pull/17836
* fix: use tags icon for `/settings/organizations/attributes` page by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17841
* feat: update translations via [@&#8203;replexica](https://github.com/replexica) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/17854
* fix: round robin host reschedule error by [@&#8203;SomayChauhan](https://github.com/SomayChauhan) in https://github.com/calcom/cal.com/pull/17855
* fix: unable to remove banner by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/17857
* feat: Salesforce - pass company booking response to the Lead field by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/17860
* fix: provide fixed widths for DataTable columns by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/17853

[1.14.11]
* Update cal.com to 4.7.13
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* fix: Routing form report filters by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/17865
* fix: (Hotfix)any group of rules in routing by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/17869
* fix: move organization members list by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/17531
* fix: load more rows if the existing ones do not fill the space by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/17874
* fix: v2 return booking with split name attendee by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/17877
* refactor: v2 return host username by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/17880
* fix: Remove double encoding for OG image URLs by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17872
* feat: add tooltip to button for better UX by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17881
* feat: hide the update button in case of no permission by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17883

[1.14.12]
* Update cal.com to 4.7.14
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* fix: Exclude locked users by default by [@&#8203;zomars](https://github.com/zomars) in https://github.com/calcom/cal.com/pull/17889
* refactor: v2 validate booking fields by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/17878
* fix: revert bg color for pinned column at data table by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/17892
* fix: remove from host when removed from org by [@&#8203;Udit-takkar](https://github.com/Udit-takkar) in https://github.com/calcom/cal.com/pull/17893
* fix: `getAttributeRoutingConfig` find the route based on the chosenRouteId by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/17867
* feat: Salesforce - send routing form responses to Salesforce by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/17890
* fix: locked users admin list and bypass query by [@&#8203;zomars](https://github.com/zomars) in https://github.com/calcom/cal.com/pull/17902

[1.14.13]
* Update cal.com to 4.7.16
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* Image tidying up

[1.14.14]
* Update cal.com to 4.7.17
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* feat: watchlist by [@&#8203;zomars](https://github.com/zomars) in https://github.com/calcom/cal.com/pull/17947
* fix: z index hover card on routing form responses by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/17981
* fix: remove duplicate headers in attribute pages by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17985
* feat: Salesforce - search for account ownership based on website by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/17989
* fix: trying to update the timeFormat throws error by [@&#8203;SomayChauhan](https://github.com/SomayChauhan) in https://github.com/calcom/cal.com/pull/17991
* refactor: v2 OAuth webhooks & workflows by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/17959
* fix: disable mandatory reminder for platform by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/17998
* feat: add assignment reason to routing insights by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/17984

[1.14.15]
* Update cal.com to 4.7.18
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* fix: fetching event-types of org users apiv2 by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/18004
* fix: custom implem markdownToSafeHTML for platform by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/18018
* feat: Add slotFormat query parameter to slots API V2 endpoint to return start and end time of a slot by [@&#8203;asadath1395](https://github.com/asadath1395) in https://github.com/calcom/cal.com/pull/17873
* fix: localStorage not found for `Booker` atom by [@&#8203;Ryukemeister](https://github.com/Ryukemeister) in https://github.com/calcom/cal.com/pull/18014
* fix: Don't force reschedule with same RR host if reschedule is actually rerouting by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/17511
* fix: days of the week in different language in the workflow by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/17850
* feat: refactor filters on insights response table by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/17796
* feat: prevent rescheduling past bookings by [@&#8203;kart1ka](https://github.com/kart1ka) in https://github.com/calcom/cal.com/pull/18003
* feat: update translations via [@&#8203;replexica](https://github.com/replexica) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/18022

[1.14.16]
* Update cal.com to 4.7.18
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* fix: fetching event-types of org users apiv2 by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/18004
* fix: custom implem markdownToSafeHTML for platform by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/18018
* feat: Add slotFormat query parameter to slots API V2 endpoint to return start and end time of a slot by [@&#8203;asadath1395](https://github.com/asadath1395) in https://github.com/calcom/cal.com/pull/17873
* fix: localStorage not found for `Booker` atom by [@&#8203;Ryukemeister](https://github.com/Ryukemeister) in https://github.com/calcom/cal.com/pull/18014
* fix: Don't force reschedule with same RR host if reschedule is actually rerouting by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/17511
* fix: days of the week in different language in the workflow by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/17850
* feat: refactor filters on insights response table by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/17796
* feat: prevent rescheduling past bookings by [@&#8203;kart1ka](https://github.com/kart1ka) in https://github.com/calcom/cal.com/pull/18003
* feat: update translations via [@&#8203;replexica](https://github.com/replexica) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/18022

[1.14.17]
* Update cal.com to 4.7.19
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* revert: "feat: refactor filters on insights response table ([#&#8203;17796](https://github.com/calcom/cal.com/issues/17796))" by [@&#8203;keithwillcode](https://github.com/keithwillcode) in https://github.com/calcom/cal.com/pull/18024
* feat: Webhook support for Managed Events by [@&#8203;alishaz-polymath](https://github.com/alishaz-polymath) in https://github.com/calcom/cal.com/pull/17986
* feat: update translations via [@&#8203;replexica](https://github.com/replexica) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/18033
* feat: Reduce bundle size via better imports + install gcal + gvideo by default for google signups by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17810

[1.14.18]
* Update cal.com to 4.7.20
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.6.15)
* feat: Okta(SCIM2.0) -> Attributes Sync and Group Option support by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/17805
* feat: v1 booking "created at" field in response by [@&#8203;alishaz-polymath](https://github.com/alishaz-polymath) in https://github.com/calcom/cal.com/pull/18038
* feat: conferencing atoms by [@&#8203;SomayChauhan](https://github.com/SomayChauhan) in https://github.com/calcom/cal.com/pull/17988
* feat: Enable SCIM logging that can be configured through constant by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/18043
* fix: Schedule a timezone change only allows you to select one day not date range by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/18045
* fix: Do not consider salesforce ownership when making rerouting decision by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/18034
* fix: When crating a new event from a team event list, the cancel button redirects to /event-types # by [@&#8203;HirenDhadhal](https://github.com/HirenDhadhal) in https://github.com/calcom/cal.com/pull/18058
* feat: domain wide delegation UI by [@&#8203;Udit-takkar](https://github.com/Udit-takkar) in https://github.com/calcom/cal.com/pull/18013
* fix: add onTouchEnd for mobile apps by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/18040
* feat: resizing in DataTable by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/17973

[1.14.19]
* Update cal.com to 4.7.21
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.7.21)
* build: Change alpine image to one compatible with Prisma by [@&#8203;emrysal](https://github.com/emrysal) in https://github.com/calcom/cal.com/pull/18080
* feat: update translations via [@&#8203;replexica](https://github.com/replexica) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/18078
* feat: ability to require an email by [@&#8203;kart1ka](https://github.com/kart1ka) in https://github.com/calcom/cal.com/pull/18057
* fix: Not able to create consecutive ooo by [@&#8203;vijayraghav-io](https://github.com/vijayraghav-io) in https://github.com/calcom/cal.com/pull/17388
* feat: remove setup-availability step in onboarding flow by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17799
* feat: Show routing reason on booking page \[CAL-4814] by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/18053

[1.14.20]
* Update cal.com to 4.7.22
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.7.22)
* feat: update translations via [@&#8203;replexica](https://github.com/replexica) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/18100
* feat: assigned bookings per period by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/17940
* fix: Insights team filter behavior by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/18084
* feat: Title - AI translation for booking pages by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/17794
* fix: prevent infinite rerendering of DataTable by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/18087
* revert: "fix: Request permissions to allow events to be created on sh by [@&#8203;keithwillcode](https://github.com/keithwillcode) in https://github.com/calcom/cal.com/pull/18111
* fix: Error on webhooks page exporting client only comp directly by [@&#8203;emrysal](https://github.com/emrysal) in https://github.com/calcom/cal.com/pull/18118
* fix: do not export client components directly in app routes by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/18122
* fix: correct line-breaks in calendar event description by [@&#8203;michaelvalette](https://github.com/michaelvalette) in https://github.com/calcom/cal.com/pull/18077
* feat: Change 'add override' to 'save override' by [@&#8203;Amit91848](https://github.com/Amit91848) in https://github.com/calcom/cal.com/pull/17434
* refactor: improved observability for Salesforce by [@&#8203;zomars](https://github.com/zomars) in https://github.com/calcom/cal.com/pull/18125
* [@&#8203;michaelvalette](https://github.com/michaelvalette) made their first contribution in https://github.com/calcom/cal.com/pull/18077

[1.14.21]
* Update cal.com to 4.7.25
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.7.25)
* fix: show tooltip below the delete button by [@&#8203;sanchitttt](https://github.com/sanchitttt) in https://github.com/calcom/cal.com/pull/18135
* fix: update get booking output for v2 by [@&#8203;Ryukemeister](https://github.com/Ryukemeister) in https://github.com/calcom/cal.com/pull/18082
* fix: Searching users in admin page not working by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/18140
* fix: default to 100 when attribute weight undefined by [@&#8203;CarinaWolli](https://github.com/CarinaWolli) in https://github.com/calcom/cal.com/pull/18145
* feat: update translations via [@&#8203;replexica](https://github.com/replexica) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/18139
* fix: Prisma validation error in generateMetadata (workflows) by [@&#8203;emrysal](https://github.com/emrysal) in https://github.com/calcom/cal.com/pull/18149
* refactor: improved observability in google calendar service by [@&#8203;zomars](https://github.com/zomars) in https://github.com/calcom/cal.com/pull/18147
* fix: ensure migrated pages are added to config.matcher in middleware by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/18153
* fix: Hyperlinks not highlighted in eventmeta by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/18120
* [@&#8203;sanchitttt](https://github.com/sanchitttt) made their first contribution in https://github.com/calcom/cal.com/pull/18135
* build: Node alpine libssl symlink fix by [@&#8203;emrysal](https://github.com/emrysal) in https://github.com/calcom/cal.com/pull/18127
* fix: Salesforce - only call create prospect record once by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/18131
* fix: CRM error log by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/18105
* fix: store default booking fields when creating event by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/18108
* feat: weights per attribute UI by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/18109
* fix: early return if no users are found to prevent prisma error by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/18133
* fix: filling users logic by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/18128

[1.15.0]
* Update cal.com to 4.8.14
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.8.14)

[1.15.1]
* Update cal.com to 4.8.15
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.8.15)
* feat: update translations via [@&#8203;replexica](https://github.com/replexica) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/18496
* perf: use react svg cache from root level by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/18506
* feat: add DataTableWrapper by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/18503
* fix: add index to AssignmentReason by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/18520
* feat: Insihts analytics app added by [@&#8203;7hakurg](https://github.com/7hakurg) in https://github.com/calcom/cal.com/pull/18461

[1.15.2]
* Update cal.com to 4.8.16
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.8.16)
* fix: payment not working in booker by [@&#8203;SomayChauhan](https://github.com/SomayChauhan) in https://github.com/calcom/cal.com/pull/18540
* fix: Bailey's minor docs  by [@&#8203;mintlify](https://github.com/mintlify) in https://github.com/calcom/cal.com/pull/18543
* feat: Mintlify /help edits by [@&#8203;mintlify](https://github.com/mintlify) in https://github.com/calcom/cal.com/pull/18555
* fix: added videos and fixed iframe sizes for help page docs by [@&#8203;nizzyabi](https://github.com/nizzyabi) in https://github.com/calcom/cal.com/pull/18558

[1.15.3]
* Update cal.com to 4.8.19
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.8.19)
* fix: disable sending sms when email is present by [@&#8203;Udit-takkar](https://github.com/Udit-takkar) in https://github.com/calcom/cal.com/pull/18632
* fix: main lint errors by [@&#8203;emrysal](https://github.com/emrysal) in https://github.com/calcom/cal.com/pull/18634
* feat: don't show zapier/make webhooks in webhook list by [@&#8203;kart1ka](https://github.com/kart1ka) in https://github.com/calcom/cal.com/pull/18607

[1.16.0]
* Update cal.com to 4.9.1
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.9.1)
* fix: Booking atom phone booking field and booking fields order by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/18678
* fix: v2 booking guests by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/18690

[1.16.1]
* Update cal.com to 4.9.2
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.9.2)
* perf: Move CRM event creation to tasker by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/18370
* fix: resize app-store images to use less bytes by [@&#8203;nizzyabi](https://github.com/nizzyabi) in https://github.com/calcom/cal.com/pull/18675
* fix: Booking with the same email when using capital letters by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/18694
* fix: filters ui breaking on mobile in routing insights page by [@&#8203;cnhhoang850](https://github.com/cnhhoang850) in https://github.com/calcom/cal.com/pull/18713
* feat: Salesforce - write to field without validation by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/18722
* fix: edit webhook time by [@&#8203;Udit-takkar](https://github.com/Udit-takkar) in https://github.com/calcom/cal.com/pull/18549
* fix: duplicating an event turns private link on by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/18727
* fix: Getting an error with managed events when accessing through api/v1 by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/18683
* fix: Impersonation issue from userstable by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/18608
* fix: potential org slug fix by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/18718
* feat: Use dub.customer.list to issue dual-sided incentives by [@&#8203;steven-tey](https://github.com/steven-tey) in https://github.com/calcom/cal.com/pull/18452

[1.16.2]
* Update cal.com to 4.9.3
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.9.3)
* fix: adjust table height for a few rows on /bookings by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/18728
* fix: small spacing fix in app router 404 by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/18741
* feat: auto lock by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/18630
* fix: Parse JSON metadata better and stop crash from API V1 by [@&#8203;alishaz-polymath](https://github.com/alishaz-polymath) in https://github.com/calcom/cal.com/pull/18756
* fix: ensure platform managed user has 1 schedule by default by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/18719
* fix: nested scrolls on /bookings/upcoming by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/18760
* feat: HitPay Payment App by [@&#8203;MuhammadAimanSulaiman](https://github.com/MuhammadAimanSulaiman) in https://github.com/calcom/cal.com/pull/17213
* fix: Disable custom templates for trialing teams by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/18748
* feat: update translations via [@&#8203;replexica](https://github.com/replexica) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/18764
* [@&#8203;MuhammadAimanSulaiman](https://github.com/MuhammadAimanSulaiman) made their first contribution in https://github.com/calcom/cal.com/pull/17213

[1.16.3]
* Update cal.com to 4.9.4
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.9.4)
* feat: Use Cloudflare Turnstile in booker by [@&#8203;keithwillcode](https://github.com/keithwillcode) in https://github.com/calcom/cal.com/pull/18755
* feat: add transcription as attachment by [@&#8203;Udit-takkar](https://github.com/Udit-takkar) in https://github.com/calcom/cal.com/pull/18740
* fix: routing form list overflow by [@&#8203;MehulZR](https://github.com/MehulZR) in https://github.com/calcom/cal.com/pull/18686
* fix: handle cancelation reason when user is host by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/18681
* fix: Initialize Hubspot client in CRMService by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/18789
* fix: custom templates on team plan by [@&#8203;CarinaWolli](https://github.com/CarinaWolli) in https://github.com/calcom/cal.com/pull/18792

[1.16.4]
* Update cal.com to 4.9.13
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v4.9.13)

[1.17.0]
* Update calcom to 5.0.5
* [Full changelog](https://github.com/calcom/cal.com/releases/tag/v5.0.5)
* checklist added to manifest

[1.17.1]
* Update cal.com to 5.0.6
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v5.0.6)
* fix: block POST requests to invalid routes in middleware intentionally by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/19446
* feat: enable_pip_ui in dailyvideo by [@&#8203;vijayraghav-io](https://github.com/vijayraghav-io) in https://github.com/calcom/cal.com/pull/19423

[1.17.2]
* Update cal.com to 5.0.7
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v5.0.7)
* perf: cache app store imports by [@&#8203;keithwillcode](https://github.com/keithwillcode) in https://github.com/calcom/cal.com/pull/19519
* perf: run `ssrInit()` at root layout by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/19460
* fix: Encode usernames in booking url to UTF-8 to prevent 500 throws by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/19475
* fix: No option to install/disconnect app from app detail page by [@&#8203;asadath1395](https://github.com/asadath1395) in https://github.com/calcom/cal.com/pull/17997
* fix: Attempted to call ZFormsInputSchema() from the server by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/19540
* feat: v2 managed organizations by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/19341
* fix: disable skip confirm for booker_layout=WEEK_VIEW by [@&#8203;SomayChauhan](https://github.com/SomayChauhan) in https://github.com/calcom/cal.com/pull/19524
* fix: refactor filter UIs on /bookings by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/19532
* fix: some ui fixes by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/19558

[1.17.3]
* Update cal.com to 5.0.10
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v5.0.10)
* fix: App theme being used in routing-form embed by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/19489
* feat: update translations via [@&#8203;lingodotdev](https://github.com/lingodotdev) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/19632
* revert: v2 managed user attendee bookings by [@&#8203;zomars](https://github.com/zomars) in https://github.com/calcom/cal.com/pull/19635
* fix: useAtomGetEventTypes pathname  by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/19637
* hotfix: Embed - Sometimes hidden mostly for acme.cal.local/team1 page by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/19639
* fix: 6 character hash suffix in the username for a member of an orga by [@&#8203;utkarsh10000](https://github.com/utkarsh10000) in https://github.com/calcom/cal.com/pull/19633
* fix: Dismiss toast on click by [@&#8203;nayan-bagale](https://github.com/nayan-bagale) in https://github.com/calcom/cal.com/pull/19650
* feat: Organization Delegation Credentials (Google,Outlook) by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/16622
* [@&#8203;utkarsh10000](https://github.com/utkarsh10000) made their first contribution in https://github.com/calcom/cal.com/pull/19633
* fix: v2 managed user attendee bookings by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/19612

[1.17.4]
* Update cal.com to 5.0.11
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v5.0.11)
* feat: update translations via [@&#8203;lingodotdev](https://github.com/lingodotdev) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/19654
* fix: Re-instate new slot logic now it's A/B tested in production by [@&#8203;emrysal](https://github.com/emrysal) in https://github.com/calcom/cal.com/pull/19605
* refactor: Move [@&#8203;calcom/core](https://github.com/calcom/core) to [@&#8203;calcom/lib](https://github.com/calcom/lib) by [@&#8203;keithwillcode](https://github.com/keithwillcode) in https://github.com/calcom/cal.com/pull/19655
* fix: verification input field  by [@&#8203;retrogtx](https://github.com/retrogtx) in https://github.com/calcom/cal.com/pull/19656
* fix: Incorrect Attendee Shown When Cancelling a Seated Event by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/19647
* fix: remove add guests from the dropdown if its disabled in the booking questions by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/19649
* feat: added Ability to set organizer's default app as a valid input location in api/v2 by [@&#8203;SomayChauhan](https://github.com/SomayChauhan) in https://github.com/calcom/cal.com/pull/19549
* fix: show selected filter values first in single- & multi-select filters by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/19620

[1.17.5]
* Update cal.com to 5.0.12
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v5.0.12)
* feat: improve Intercom app by [@&#8203;vachmara](https://github.com/vachmara) in https://github.com/calcom/cal.com/pull/19687
* fix: api v2 axiom logs exceed column size by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/19636
* fix: member filter issue by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/19707
* fix: incorrect locale value by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/19692
* feat: improved ooo search by [@&#8203;vijayraghav-io](https://github.com/vijayraghav-io) in https://github.com/calcom/cal.com/pull/19704
* fix: v2 managed user attendee bookings by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/19716
* feat: update translations via [@&#8203;lingodotdev](https://github.com/lingodotdev) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/19729
* perf: dynamic import for Sentry CaptureException by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/19730
* fix: typos in packages directory by [@&#8203;luzpaz](https://github.com/luzpaz) in https://github.com/calcom/cal.com/pull/19574
* fix: auto submission upon uploading photo by [@&#8203;retrogtx](https://github.com/retrogtx) in https://github.com/calcom/cal.com/pull/19715

[1.17.6]
* Update cal.com to 5.0.13
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v5.0.13)
* refactor: Remove intervalLimits from [@&#8203;calcom/lib](https://github.com/calcom/lib) and export directly by [@&#8203;emrysal](https://github.com/emrysal) in https://github.com/calcom/cal.com/pull/19710
* feat: add booking id & booking link columns to /insights/routing by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/19697
* fix: prevent background scroll on opening timezone select menu by [@&#8203;suraj719](https://github.com/suraj719) in https://github.com/calcom/cal.com/pull/19789
* feat: display country code from IP by [@&#8203;Udit-takkar](https://github.com/Udit-takkar) in https://github.com/calcom/cal.com/pull/19788
* fix: invalid access token bug for v2 bookings controller by [@&#8203;Ryukemeister](https://github.com/Ryukemeister) in https://github.com/calcom/cal.com/pull/19793
* refactor: Salesforce - account query based on website by [@&#8203;joeauyeung](https://github.com/joeauyeung) in https://github.com/calcom/cal.com/pull/19799
* fix: api v2 create event-type with delegation credentials by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/19797
* [@&#8203;suraj719](https://github.com/suraj719) made their first contribution in https://github.com/calcom/cal.com/pull/19789

[1.17.7]
* Update cal.com to 5.0.14
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v5.0.14)
* docs: update SAML guide by [@&#8203;alishaz-polymath](https://github.com/alishaz-polymath) in https://github.com/calcom/cal.com/pull/19778
* docs: Update README to include NODE_OPTIONS script by [@&#8203;retrogtx](https://github.com/retrogtx) in https://github.com/calcom/cal.com/pull/19815
* fix: dry run improvements for v2  by [@&#8203;Ryukemeister](https://github.com/Ryukemeister) in https://github.com/calcom/cal.com/pull/19196
* fix: fixes the crash when empty `users` or `hosts` array is passed to \_getDelegationCredentialsMapPerUser by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/19834
* fix: Cannot Read properties of undefined reading ('id')  by [@&#8203;harshit078](https://github.com/harshit078) in https://github.com/calcom/cal.com/pull/19810
* revert: "fix: prevent background scroll on opening timezone select menu" by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/19814
* fix: Calendar events attached to workflow emails are missing meeting url by [@&#8203;anikdhabal](https://github.com/anikdhabal) in https://github.com/calcom/cal.com/pull/19790
* feat: team ooo read-only for non-admin by [@&#8203;vijayraghav-io](https://github.com/vijayraghav-io) in https://github.com/calcom/cal.com/pull/19700
* fix: calcom UI circle dep by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/19839
* fix: date range on /bookings by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/19785
* fix: failing tests during DST transition by [@&#8203;vijayraghav-io](https://github.com/vijayraghav-io) in https://github.com/calcom/cal.com/pull/19862
* feat: no-show confirmation does not pop up for single attendee by [@&#8203;retrogtx](https://github.com/retrogtx) in https://github.com/calcom/cal.com/pull/19806
* fix: Admin orgs listing crash by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/19825
* fix: Typo in Email Address by [@&#8203;suyash5053](https://github.com/suyash5053) in https://github.com/calcom/cal.com/pull/19879
* fix: playwright typos by [@&#8203;luzpaz](https://github.com/luzpaz) in https://github.com/calcom/cal.com/pull/19794
* refactor: extract slots from libraries barrel file  by [@&#8203;Ryukemeister](https://github.com/Ryukemeister) in https://github.com/calcom/cal.com/pull/19895
* fix: profile to user repository circle dependancy  by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/19892
* fix: calcom trpc circle deps slots handlers by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/19896
* fix: add documentaion to `blockUnconfirmedBookingsInBooker` by [@&#8203;SomayChauhan](https://github.com/SomayChauhan) in https://github.com/calcom/cal.com/pull/19894
* fix: platform libraries barrel file extract event-types by [@&#8203;SomayChauhan](https://github.com/SomayChauhan) in https://github.com/calcom/cal.com/pull/19846
* refactor: circular dependencies platform packages by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/19903
* perf: Reduce imports by moving bookings zod out of utils by [@&#8203;keithwillcode](https://github.com/keithwillcode) in https://github.com/calcom/cal.com/pull/19869
* feat: update platform onboarding docs by [@&#8203;Ryukemeister](https://github.com/Ryukemeister) in https://github.com/calcom/cal.com/pull/17941
* fix: add badge for disabled workflow actions and templates in trial mode by [@&#8203;CarinaWolli](https://github.com/CarinaWolli) in https://github.com/calcom/cal.com/pull/19675
* fix: removed rounded top corners on webhook form by [@&#8203;avnish100](https://github.com/avnish100) in https://github.com/calcom/cal.com/pull/19905
* fix: update filter styles on org member list by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/19918
* feat: round robin reset interval for teams by [@&#8203;CarinaWolli](https://github.com/CarinaWolli) in https://github.com/calcom/cal.com/pull/19441
* fix: duplicate value found in common.json error by [@&#8203;retrogtx](https://github.com/retrogtx) in https://github.com/calcom/cal.com/pull/19693
* feat: update translations via [@&#8203;lingodotdev](https://github.com/lingodotdev) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/19927
* [@&#8203;Bashamega](https://github.com/Bashamega) made their first contribution in https://github.com/calcom/cal.com/pull/19867
* [@&#8203;avnish100](https://github.com/avnish100) made their first contribution in https://github.com/calcom/cal.com/pull/19905

[1.17.8]
* Update cal.com to 5.0.17
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v5.0.17)
* fix: catch error for getOwnerId with invalid token by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/19949
* fix: correct translation key for host and attendee emails by [@&#8203;retrogtx](https://github.com/retrogtx) in https://github.com/calcom/cal.com/pull/19962
* fix: update wrong oauth tests and parseRequestData util  by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/19958
* feat: calendar-links API by [@&#8203;hariombalhara](https://github.com/hariombalhara) in https://github.com/calcom/cal.com/pull/19777
* feat: v2 get managed users by email by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/19526
* fix: v2 team metadata by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/19939
* fix: do not export client components directly from RSC by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/19971
* fix: apply standard pagination to /insights/routing by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/19950
* fix: v2 rescheduled booking with confirmation by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/19833
* refactor: extract schedules from libraries barrel file by [@&#8203;Ryukemeister](https://github.com/Ryukemeister) in https://github.com/calcom/cal.com/pull/19838
* fix: broken metadata import from platform/types by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/19986
* fix: mobile-view by [@&#8203;TusharBhatt1](https://github.com/TusharBhatt1) in https://github.com/calcom/cal.com/pull/19928
* feat: sound on browser push notification by [@&#8203;Udit-takkar](https://github.com/Udit-takkar) in https://github.com/calcom/cal.com/pull/18548
* fix: handle both json and form data correctly in POST methods by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/19930
* fix: extracted `app-store` and `DWD` platform-libraries imports to a `/app-store` barrel file by [@&#8203;SomayChauhan](https://github.com/SomayChauhan) in https://github.com/calcom/cal.com/pull/19938
* feat: implement standard pagination for org member list by [@&#8203;eunjae-lee](https://github.com/eunjae-lee) in https://github.com/calcom/cal.com/pull/19802
* refactor: extract emails from libraries barrel file by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/19836
* test: managed org creator profiles by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/19944
* perf: Remove dayjs import from main constants.ts file by [@&#8203;keithwillcode](https://github.com/keithwillcode) in https://github.com/calcom/cal.com/pull/19644

[1.17.9]
* Update cal.com to 5.0.18
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v5.0.18)
* feat: add sentry report wrapper to every api route in appDir by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/19966
* feat: update translations via [@&#8203;lingodotdev](https://github.com/lingodotdev) by [@&#8203;calcom-bot](https://github.com/calcom-bot) in https://github.com/calcom/cal.com/pull/19992
* fix: importing from [@&#8203;calcom/emails](https://github.com/calcom/emails) exposed daily-video api route to client code by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/19994

[1.17.10]
* Update cal.com to 5.0.19
* [Full Changelog](https://github.com/calcom/cal.com/releases/tag/v5.0.19)
* fix: push notification page UI by [@&#8203;Udit-takkar](https://github.com/Udit-takkar) in https://github.com/calcom/cal.com/pull/19999
* fix: add improved logging and decrease ttl by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/19979
* fix: avatar sentry errors by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/20005
* fix: remove Trans component by [@&#8203;Udit-takkar](https://github.com/Udit-takkar) in https://github.com/calcom/cal.com/pull/20008
* refactor: v2 booking locations by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/19613
* fix: fixes extra span causing issues in seated event display by [@&#8203;sean-brydon](https://github.com/sean-brydon) in https://github.com/calcom/cal.com/pull/20004
* docs: v2 reschedule and cancel booking by [@&#8203;supalarry](https://github.com/supalarry) in https://github.com/calcom/cal.com/pull/20011
* fix: api-v2 libraries/app-store missing tsconfig by [@&#8203;ThyMinimalDev](https://github.com/ThyMinimalDev) in https://github.com/calcom/cal.com/pull/20006
* fix: round robin locale err by [@&#8203;retrogtx](https://github.com/retrogtx) in https://github.com/calcom/cal.com/pull/20024
* fix: no-meeting-found is exporting client component from RSC by [@&#8203;hbjORbj](https://github.com/hbjORbj) in https://github.com/calcom/cal.com/pull/20027
* fix: hide slot unavailable message for instant meetings by [@&#8203;Udit-takkar](https://github.com/Udit-takkar) in https://github.com/calcom/cal.com/pull/20002

